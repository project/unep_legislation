<?php

declare(strict_types=1);

namespace Drupal\indigo_data_provider\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\indigo_data_provider\IndigoDataProviderInterface;

/**
 * Defines the indigo data provider entity type.
 *
 * @ConfigEntityType(
 *   id = "indigo_data_provider",
 *   label = @Translation("Indigo Data Provider"),
 *   label_collection = @Translation("Indigo Data Providers"),
 *   label_singular = @Translation("indigo data provider"),
 *   label_plural = @Translation("indigo data providers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count indigo data provider",
 *     plural = "@count indigo data providers",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\indigo_data_provider\IndigoDataProviderListBuilder",
 *     "form" = {
 *       "add" = "Drupal\indigo_data_provider\Form\IndigoDataProviderForm",
 *       "edit" = "Drupal\indigo_data_provider\Form\IndigoDataProviderForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "indigo_data_provider",
 *   admin_permission = "administer indigo_data_provider",
 *   links = {
 *     "collection" = "/admin/structure/indigo-data-provider",
 *     "add-form" = "/admin/structure/indigo-data-provider/add",
 *     "edit-form" = "/admin/structure/indigo-data-provider/{indigo_data_provider}",
 *     "delete-form" = "/admin/structure/indigo-data-provider/{indigo_data_provider}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "url",
 *     "api_url",
 *     "api_key",
 *   },
 * )
 */
final class IndigoDataProvider extends ConfigEntityBase implements IndigoDataProviderInterface {

  protected string $id;

  protected string $name;

  protected ?string $url;

  protected ?string $api_url;

  protected ?string $api_key;

}
