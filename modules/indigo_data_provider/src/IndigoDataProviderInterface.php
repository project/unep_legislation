<?php declare(strict_types = 1);

namespace Drupal\indigo_data_provider;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an indigo data provider entity type.
 */
interface IndigoDataProviderInterface extends ConfigEntityInterface {

}
