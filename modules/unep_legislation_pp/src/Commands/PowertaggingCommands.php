<?php

namespace Drupal\unep_legislation_pp\Commands;

use Dflydev\DotAccessData\Util;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\semantic_connector\Entity\SemanticConnectorPPServerConnection;
use Drupal\unep_legislation_pp\Utils;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class PowertaggingCommands {

  use StringTranslationTrait;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a PowertaggingCommands object.
   *
   * @param LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LoggerChannelFactoryInterface $logger, RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    $this->logger = $logger->get('powertagging');
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * A custom Drush command to tag legislation at the paragraph level.
   *
   * @command unep-legislation:tag-paragraphs
   *
   * @param string $pp_server_id
   *    The powertagging config ID.
   * @param string $pp_project_uuid
   *    The project's UUID from PoolParty platform.
   * @param string $field_markup
   *    The Drupal field name where the markup to be extracted is stored.
   * @param string $field_json_destination
   *    The Drupal field where the resulting JSON is saved.
   * @param string $field_tag_entity_destination
   *    The Drupal field where the resulting term entities are stored. Resulting tids are put to this entity_reference field.
   * @param string $drupal_pp_taxonomy
   *    The machine name of the taxonomy where the PoolParty project is synchronized.
   * @param float $score_threshold
   *    The minimum score for concepts to be saved.
   * @param string $entity_type_id
   *    The entity type ID to be tagged.
   * @param string $bundle
   *    The bundle of the entity to be tagged.
   * @param int $nid
   *    Tag only specific node.
   *
   * @example drush unep-legislation:tag-paragraphs brs_dev_server 940c5505-28f7-447b-8302-3bcfaefdb0ac field_markup field_paragraph_tags climate_change_toolkit 0.1 node legislation
   *   <info>Tag</info> all legislation nodes, extract markup from field_markup, store tags in field_paragraph_tags, use terms from climate_change_toolkit taxonomy, only terms with score greater than 0.1
   *
   *
   * @return int|void
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   *
   */
  public function tagContent(string $pp_server_id, string $pp_project_uuid,
        string $field_markup, string $field_json_destination, string $field_tag_entity_destination, string $drupal_pp_taxonomy,
        float $score_threshold = 0.1, string $entity_type_id='node', string $bundle='akn_legislation', $nid = NULL) {
    $language = 'en'; // @todo
    // $pp_project_uuid = '940c5505-28f7-447b-8302-3bcfaefdb0ac';
    // $field_destination = 'field_paragraph_tags';
    // $field_markup = 'field_markup';
    // $drupal_pp_taxonomy = 'climate_change_toolkit';
    // $score_threshold = 0.1;

    /** @var \Drupal\semantic_connector\Entity\SemanticConnectorPPServerConnection $ppServer */
    if (!$ppServer = SemanticConnectorPPServerConnection::load($pp_server_id)) {
      $this->logger->error($this->t('Unknown PP server @config', [
        '@config' => $pp_server_id,
      ]));
      return -1;
    }
    $bundle_key = $this->entityTypeManager->getDefinition($entity_type_id)->getKey('bundle');
    $entity_pk = $this->entityTypeManager->getDefinition($entity_type_id)->getKey('id');
    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery()
      ->condition($bundle_key, $bundle)->accessCheck(FALSE);
    if(!empty($nid)) {
      $query->condition($entity_pk, $nid);
    }
    $entities = $query->execute();
    if (empty($entities)) {
      $this->logger->notice($this->t('No entities to tag'));
      return;
    }
    $this->logger->notice($this->t('Started tagging @count entities', [
      '@count' => count($entities),
    ]));

    $default_options = [
      'projectId' => $pp_project_uuid,
      'numberOfTerms' => 0,
      'showMatchingDetails' => 1,
      'showMatchingPosition' => 1,
      'numberOfConcepts' => 999,
      'displayText' => 0,
      'categorize' => 1,
    ];

    $idx = 0;
    foreach ($entities as $entity) {
      $idx++;
      if(!$entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity)) {
        $this->logger->error($this->t('Cannot load entity @entity_type @entity_id',
          ['@entity_type' => $entity_type_id, '@entity_id' => $entity]));
        continue;
      }
      try {
        $this->logger->notice($this->t('Processing @entity_type #@entity_id (@index/@count)', [
          '@entity_type' => $entity_type_id,
          '@entity_id' => $entity->id(),
          '@index' => $idx,
          '@count' => count($entities),
        ]));
        $markup = trim($entity->get($field_markup)->value);
        if (empty($markup)) {
          $this->logger->warning($this->t('The @entity #@id does not have markup in field @field', ['@entity' => $entity_type_id, '@id' => $entity->id(), '@field' => $field_markup]));
          continue;
        }
        $parts = Utils::extractTagsWithClass($markup);
        $paragraph_tags = [];
        $this->logger->notice($this->t('Tagging @count paragraphs', ['@count' => count($parts)]));
        foreach($parts as $id => $part) {
          echo ".";
          $response = $ppServer->getApi()->extractConcepts($part, $language, $default_options, 'text');
          $tags = Utils::processTaggerResponse($response, $this->entityTypeManager, 'taxonomy_term', $drupal_pp_taxonomy, $score_threshold, $this->logger);
          if(!empty($tags)) {
            $paragraph_tags[$id] = $tags;
          }
        }
        echo "\n";
        if(!empty($paragraph_tags)) {
          $tids = [];
          var_dump($paragraph_tags);
          foreach($paragraph_tags as $tags) {
            foreach($tags as $tag) {
              $tid = $tag['tid'];
              $root_tid = $tag['root']['tid'];
              $tids[$tid] = ['target_id' => $tid];
              $tids[$root_tid] = ['target_id' => $root_tid];
            }
          }
          var_dump(array_keys($tids));
          $entity->set($field_json_destination, json_encode($paragraph_tags));
          $entity->set($field_tag_entity_destination, $tids);
          $entity->save();
        }
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
    }

    $this->logger->notice($this->t('Successfully tagged @count entities.', [
      '@count' => $idx,
    ]));
  }
}