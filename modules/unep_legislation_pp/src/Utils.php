<?php

namespace Drupal\unep_legislation_pp;

use DOMDocument;
use DOMXPath;
use Drupal\taxonomy\Entity\Term;

class Utils {

  public static function extractTagsWithClass($html, $tag='span', $class='akn-p'): array {
    $dom = new DOMDocument;
    libxml_use_internal_errors(true);
    $dom->loadHTML($html);
    libxml_clear_errors();
    $xpath = new DOMXPath($dom);
    $query = sprintf("//%s[contains(@class, '%s')]", $tag, $class);
    $tags = $xpath->query($query);
    $result = [];
    foreach ($tags as $tag) {
      // @todo better error handling and logging
      $id = $tag->getAttribute('id');
      $result[$id] = $dom->saveHTML($tag);
    }
    return $result;
  }

  public static function processTaggerResponse($response, $entityTypeManager, $entity_type_id, $bundle, $score_threshold, $logger): array {
    $categories = !empty($response['categories']) ? $response['categories'] : [];
    $bundle_key = $entityTypeManager->getDefinition($entity_type_id)->getKey('bundle');
    $ret = [];
    foreach($categories as $category) {
      $score = $category['score'] ?: 0;
      if ($score < $score_threshold) {
        // @todo better error handling and logging
        continue;
      }
      $uri = $category['uri'];
      $terms = $entityTypeManager->getStorage($entity_type_id)->getQuery()
        ->condition($bundle_key, $bundle)
        ->condition('field_uri', $uri) // @todo check if always field_uri
        ->accessCheck(FALSE)->execute();
      if($tid = reset($terms)) {
        $root = self::getRootParentTerm($tid);
        $root_uri = $root->get('field_uri')->getValue()[0]['uri'];
        $ob = [
          'tid' => $tid,
          'uri' => $uri,
          'type' => 'TopConcept',
          'score' => $score,
          'labels' => [
            'en' => $category['prefLabel']
          ],
          'root' => [
            'tid' => $root->id(),
            'uri' => $root_uri,
            'labels' => ['en' => $root->label()],
          ]
        ];
        $ret[] = $ob;
      }
    }
    return $ret;
  }


  /**
   * Function to get the root parent of a term.
   *
   * @param int $tid
   *   The term ID of the term for which to find the root parent.
   *
   * @return \Drupal\taxonomy\Entity\Term|null
   *   The root parent term or NULL if no parent is found.
   */
  public static function getRootParentTerm(int $tid): ?Term {
    if ($term = Term::load($tid)) {
      while ($term->get('parent')->getEntity()) {
        $parents = $term->get('parent')->referencedEntities();
        if (!empty($parents)) {
          $term = reset($parents);
        } else {
          break;
        }
      }
      return $term;
    }
    return NULL;
  }
}
