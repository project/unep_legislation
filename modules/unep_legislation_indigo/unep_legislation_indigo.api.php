<?php

/**
 * @file
 * Hooks and documentation related to the UNEP Indigo import.
 */

/**
 * Add extra values to node before validation.
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node to alter.
 * @param \Drupal\unep_legislation_indigo\Work $work
 *   The Indigo work expression.
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function hook_preprocess_indigo_node(NodeInterface &$node, $work) {
  // For example, populate a required field before validation.
  $node->set('field_climate_toolkit_tags', []);
}

/**
 * Alter Indigo terms before save.
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function hook_preprocess_indigo_terms($work, &$enrichment) {
}
