<?php

namespace Drupal\unep_legislation_indigo\Client;

use Drupal\unep_legislation_indigo\Drupal\ImportUtil;

class Work {
  /**
   * A Work is a piece of legislation, such as an act, regulation or by-law. A
   * work may be amended over time and may even have its title changed. A work
   * is uniquely identified by a work FRBR URI which never changes.
   * @var string
   */
  private string $frbr_uri;

  private IndigoInterface $client;

  public string $title;
  public ?string $created_at;
  public ?string $updated_at;
  public ?string $country;
  public ?string $locality;
  public ?string $nature;
  public ?string $subtype;
  public ?string $year;
  public ?string $number;
  public ?string $expression_frbr_uri;
  public ?string $publication_date;
  public ?string $publication_name;
  public ?string $publication_number;
  public ?object $publication_document;
  public ?bool $commenced;
  public ?bool $commenced_in_full;
  public ?string $commencement_date;
  public ?object $commencing_work;
  public ?string $assent_date;
  public ?object $parent_work;
  public ?string $expression_date;
  public ?string $language;
  public ?array $points_in_time;
  private ?array $expressions = [];
  public ?string $amendments;
  public ?string $stub;
  public ?bool $principal;
  public ?object $repeal;
  public ?string $numbered_title;
  public ?array $taxonomy_topics = [];
  public ?array $links;
  public ?array $custom_properties;
  public ?string $work_title;
  public ?array $work_amendments;
  public ?string $type_name;

  /**
   * @todo Complete list of https://en.wikipedia.org/wiki/ISO_639-3 mappings.
   *
   * @var array|string[]
   */
  public static array $indigo2DrupalLangCode = [
    'ara' => 'ar',
    'deu' => 'de',
    'eng' => 'en',
    'fra' => 'fr',
    'rus' => 'ru',
    'spa' => 'es',
    'sqi' => 'al',
    'zho' => 'zh-hans',
  ];

  /**
   * @throws \Exception
   */
  public static function fromJson($object, IndigoInterface $client): Work {
    $work = new static();
    $work->setClient($client);
    $attributes = [
      'title' => 'title',
      'created_at' => 'created_at',
      'updated_at' => 'updated_at',
      'country' => 'country',
      'locality' => 'locality',
      'nature' => 'nature',
      'subtype' => 'subtype',
      'year' => 'year',
      'number' => 'number',
      'stub' => 'stub',
      'principal' => 'principal',
      'work_title' => 'work_title',
      'repeal' => 'repeal',
      'frbr_uri' => 'frbr_uri',
      'expression_date' => 'expression_date',
      'expression_frbr_uri' => 'expression_frbr_uri',
      'publication_date' => 'publication_date',
      'publication_name' => 'publication_name',
      'publication_number' => 'publication_number',
      'publication_document' => 'publication_document',
      'commenced' => 'commenced',
      'commenced_in_full' => 'commenced_in_full',
      'commencement_date' => 'commencement_date',
      'commencing_work' => 'commencing_work',
      'work_amendments' => 'work_amendments',
      'points_in_time' => 'points_in_time',
      'taxonomy_topics' => 'taxonomy_topics',
      'links' => 'links',
      'numbered_title' => 'numbered_title',
      'parent_work' => 'parent_work',
      'type_name' => 'type_name',
      'assent_date' => 'assent_date'
    ];
    foreach ($attributes as $local => $remote) {
      if (isset($object->$remote)) {
        $work->$local = $object->$remote;
      }
      else {
        $work->$local = NULL;
      }
    }
    if ($client->getSourceName()) {
      $work->sourceName = $client->getSourceName();
    }
    if (empty($work->taxonomy_topics)) {
      $work->taxonomy_topics = [];
    }
    $work->language = ImportUtil::getLanguageCode($object->language);
    return $work;
  }

  /**
   * @throws \Exception
   */
  public static function enrichmentsDatasetFromJson($object, IndigoInterface $client): object {
    $work = new static();
    $attributes = [
      'id' => 'id',
      'name' => 'name',
      'description' => 'description',
      'url' => 'url',
      'created_at' => 'created_at',
      'updated_at' => 'updated_at',
    ];
    foreach($attributes as $local => $remote) {
      if (isset($object->$remote)) {
        $work->$local = $object->$remote;
      } else {
        $work->$local = NULL;
      }
    }
    return $work;
  }

  public function getExpressions(): array {
    if (!is_array($this->expressions) || empty($this->expressions)) {
      return [];
    }
    ksort($this->expressions);
    return $this->expressions;
  }

  /**
   * @throws \Exception
   */
  public function addExpression(Work $expression) {
    $date = $expression->expression_date;
    $language = $expression->language;
    if (isset($this->expressions[$date][$language])) {
      throw new \Exception(
        sprintf('Duplicate expression %s - %s - %s', $expression->frbr_uri, $date, $language)
      );
    }
    $this->expressions[$date][$language] = $expression;
  }

  public function getFilePdfUrl(): ?string {
    foreach($this->links as $link) {
      if($link->mediaType == 'application/pdf') {
        return $link->href;
      }
    }
    return NULL;
  }

  public function getMarkupUrl(): ?string {
    foreach($this->links as $link) {
      if($link->mediaType == 'text/html' && $link->title == 'HTML') {
        return $link->href . '?resolver=none';
      }
    }
    return NULL;
  }

  public function getPointsInTime(): ?array {
    return $this->points_in_time;
  }

  public function getTocUrl() {
    foreach($this->links as $link) {
      if($link->mediaType == 'application/json' && $link->rel == 'toc') {
        return $link->href;
      }
    }
    return NULL;
  }

  public function getTimelineUrl(): string {
    return sprintf('%s%s/timeline.json', $this->client->getApiUrl(),  $this->frbr_uri);
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getCreatedAt(): string {
    return $this->created_at;
  }

  /**
   * @return string
   */
  public function getUpdatedAt(): string {
    return $this->updated_at;
  }

  /**
   * @return string
   */
  public function getCountry(): string {
    return $this->country;
  }

  /**
   * @return string
   */
  public function getLocality(): string {
    return $this->locality;
  }

  /**
   * @return string
   */
  public function getNature(): string {
    return $this->nature;
  }

  /**
   * @return string
   */
  public function getSubtype(): string {
    return $this->subtype;
  }

  /**
   * @return string
   */
  public function getYear(): string {
    return $this->year;
  }

  /**
   * @return string
   */
  public function getNumber(): string {
    return $this->number;
  }

  /**
   * @return string
   */
  public function getFrbrUri(): string {
    return $this->frbr_uri;
  }

  /**
   * @return string
   */
  public function getExpressionFrbrUri(): string {
    return $this->expression_frbr_uri;
  }

  /**
   * @return string
   */
  public function getPublicationDate(): string {
    return $this->publication_date;
  }

  /**
   * @return string
   */
  public function getPublicationName(): string {
    return $this->publication_name;
  }

  /**
   * @return string
   */
  public function getPublicationNumber(): string {
    return $this->publication_number;
  }

  /**
   * @return string
   */
  public function getPublicationDocument(): ?object {
    return $this->publication_document;
  }

  /**
   * @return bool
   */
  public function getCommenced(): bool {
    return $this->commenced;
  }

  /**
   * @return bool
   */
  public function getCommencedInFull(): bool {
    return $this->commenced_in_full;
  }
  /**
   * @return string
   */
  public function getCommencementDate(): string {
    return $this->commencement_date;
  }

  /**
   * @return object
   */
  public function getCommencingWork(): object {
    return $this->commencing_work;
  }

  /**
   * @return object
   */
  public function getWorkAmendments(): object {
    return $this->work_amendments;
  }

  /**
   * @return string
   */
  public function getAssentDate(): string {
    return $this->assent_date;
  }

  /**
   * @return string
   */
  public function getParentWork(): string {
    return $this->parent_work;
  }

  /**
   * @return string
   */
  public function getExpressionDate(): string {
    return $this->expression_date;
  }

  /**
   * This method always returns ISO 3166-2 code (not Indigo 639-3 code).
   *
   * @return ?string
   */
  public function getLanguage(): ?string {
    return $this->language;
  }

  /**
   * @return string
   */
  public function getAmendments(): string {
    return $this->amendments;
  }

  /**
   * @return string
   */
  public function getStub(): string {
    return $this->stub;
  }

  /**
   * @return bool
   */
  public function isPrincipal(): bool {
    return $this->principal;
  }

  /**
   * @return object
   */
  public function getRepeal(): object {
    return $this->repeal;
  }

  /**
   * @return string
   */
  public function getWorkTitle(): string {
    return $this->work_title;
  }

  /**
   * @return string
   */
  public function getNumberedTitle(): string {
    return $this->numbered_title;
  }

  /**
   * @return array
   */
  public function getLinks(): array {
    return $this->links;
  }

  /**
   * @return array
   */
  public function getCustomProperties(): array {
    return $this->custom_properties;
  }

  /**
   * @return string
   */
  public function getTypeName(): string {
    return $this->type_name;
  }

  public function getClient(): IndigoInterface {
    return $this->client;
  }

  public function setClient(IndigoInterface $client) {
    $this->client = $client;
  }

  /**
   * Add term to taxonomy topics list.
   *
   * @param $topics
   *   Unique short name (code) for the topic.
   */
  public function addSlugToTaxonomyTopicsList($topics) {
    if (empty($this->taxonomy_topics) || !in_array($topics, $this->taxonomy_topics)) {
      $this->taxonomy_topics[] = $topics;
    }
    foreach ($this->getExpressions() as &$expression) {
      if (empty($expression[$this->language]->taxonomy_topics) || !in_array($topics, $expression[$this->language]->taxonomy_topics)) {
        $expression[$this->language]->taxonomy_topics[] = $topics;
      }
    }
  }
}
