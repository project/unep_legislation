<?php

namespace Drupal\unep_legislation_indigo\Client;

interface IndigoInterface {

  public function getApiUrl(): string;

  /**
   * Retrieve the list of works from Indigo.
   * Reference: https://informea.indigo.ipbes.net/api/v3/schema/swagger-ui#/work-expressions/work_expressions_list
   *
   * @param string $format
   * @param array $filters Filter results. Supported filters:
   *  - page
   *  - page_size
   *  - created_at
   *  - created_at__gte
   *  - created_at__lte
   *  - updated_at
   *  - updated_at__gte
   *  - updated_at__lte
   *
   * @return array
   * @throws \Exception
   */
  public function getWorks(string $format = 'json', array $filters = []): array;

  /**
   * Retrieve full content for a single work from Indigo. A work can have one
   * or more points in time.
   *
   * @param string $frbr_uri
   *
   * @return \Drupal\unep_legislation_indigo\Client\Work|null
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getWork(string $frbr_uri): Work|null;
  /**
   * Retrieve content for a single expression from Indigo. Expressions do not
   * have points in time defined.
   *
   * @param string $frbr_uri
   *
   * @return \Drupal\unep_legislation_indigo\Client\Work
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getExpression(string $frbr_uri): Work;

  /**
   * Download digital file (PDF, ePub, XML, HTML etc.) from Indigo.
   *
   * @param string $url
   *
   * @return string|null
   */
  public function downloadContent(string $url): ?string;

  /**
   * Retrieve the list of enrichment from Indigo.
   * Reference: https://informea.indigo.ipbes.net/api/v3/schema/swagger-ui#/enrichment-datasets/enrichment_datasets_retrieve
   *
   * @param string $endpoint
   *
   * @return array
   * @throws \Exception
   */
  public function getEnrichments(string $endpoint): ?array;
}
