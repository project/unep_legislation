<?php

namespace Drupal\unep_legislation_indigo\Client;

use Psr\Log\LoggerInterface;

/**
 *
 */
class IndigoV3 implements IndigoInterface {

  protected string $apiUrl;
  protected string $apiToken;
  protected ?string $sourceName;
  protected LoggerInterface $logger;

  public function __construct(string $apiUrl, string $apiToken, $logger, ?string $sourceName) {
    $this->apiToken = $apiToken;
    $this->apiUrl = $apiUrl;
    $this->sourceName = $sourceName;
    $this->logger = $logger;
  }

  /**
   *
   */
  public function getApiUrl(): string {
    return $this->apiUrl;
  }

  public function getSourceName(): string {
    return $this->sourceName;
  }

  /**
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function getWorks(string $format = 'json', array $filters = ['page_size' => 100]): array {
    $filters['format'] = $format;
    $works = [];
    $page = 1;
    $total = 0;
    do {
      $filters['page'] = $page;
      $query = http_build_query($filters);
      $url = $this->apiUrl . '/work-expressions?' . $query;
      $data = $this->downloadContent($url);
      $data = json_decode($data);
      $total += count($data->results);
      foreach ($data->results as $expression) {
        $expression_object = Work::fromJson($expression, $this);
        if (isset($works[$expression->frbr_uri])) {
          $this->logger->debug(sprintf('Found existing work: %s', $expression->frbr_uri));
          /** @var Work $work */
          $work = $works[$expression->frbr_uri];
        }
        else {
          $work = $expression_object;
          $works[$expression->frbr_uri] = $work;
        }
        try {
          $work->addExpression($expression_object);
        }
        catch (\Exception $e) {
          $this->logger->error($e->getMessage());
        }
      }
      $page++;
    } while ($data->next);
    $this->logger->debug(sprintf('Processed expressions: %d', $total));
    $this->logger->debug(sprintf('Total unique works: %d', count($works)));
    return $works;
  }

  /**
   * List of work expressions for a taxonomy topic.
   *
   * @param $expression_uri
   *   The URI expressions for a taxonomy topic.
   *
   * @return array
   * @throws \Exception
   */
  public function getLegislations($expression_uri, string $slug = NULL, string $format = 'json', array $filters = ['page_size' => 100]) {
    $url = $this->apiUrl . $expression_uri;
    parse_str(parse_url($url, PHP_URL_QUERY), $query);
    $filters = array_merge($filters, $query);
    $filters['format'] = $format;
    $page = 1;
    $work_array = [];
    do {
      $filters['page'] = $page;
      $query = http_build_query($filters);
      $url = $this->apiUrl . parse_url($expression_uri, PHP_URL_PATH) . '?' . $query;
      $data = $this->downloadContent($url);
      $data = json_decode($data);
      foreach ($data->results as &$result) {
        // This is important for preprocess_work_expressions_list hook in case
        // we filter by slug and extra conditions.
        $this->addSlugToTaxonomyTopicsList($result, $slug);
        $work_array[$result->frbr_uri] = $result;
      }
      $page++;
    } while ($data->next);
    return $work_array;
  }

  /**
   * Add slug term to taxonomy topics list.
   *
   * Some expression may not have the topic.
   *
   * @param mixed $work
   *   The work.
   * @param string|null $slug
   *   Unique short name (code) for the topic.
   */
  function addSlugToTaxonomyTopicsList(mixed &$work, string $slug = NULL): void {
    if (!$slug) {
      return;
    }
    if (empty($work->taxonomy_topics) || !in_array($slug, $work->taxonomy_topics)) {
      $work->taxonomy_topics[] = $slug;
    }
  }

  /**
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function getWork(string $frbr_uri): Work|null {
    $url = $this->apiUrl . $frbr_uri;
    $data = $this->downloadContent($url);
    if (empty($data)) {
      return NULL;
    }
    $data = json_decode($data);
    $work = Work::fromJson($data, $this);
    $points = $work->getPointsInTime();
    if (!empty($points)) {
      foreach ($points as $dates) {
        foreach ($dates->expressions as $expr_meta) {
          $url = $this->apiUrl . $expr_meta->expression_frbr_uri;
          $data = $this->downloadContent($url);
          if (empty($data)) {
            return NULL;
          }
          $data = json_decode($data);
          $work->addExpression(Work::fromJson($data, $this));
        }
      }
    }
    else {
      $work->addExpression($work);
    }
    return $work;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Exception
   */
  public function getExpression(string $frbr_uri): Work {
    $url = $this->apiUrl . $frbr_uri;
    $data = $this->downloadContent($url);
    $data = json_decode($data);
    return Work::fromJson($data, $this);
  }

  /**
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function downloadContent(string $url): ?string {
    $options = [
      'timeout' => 30.0,
      'follow_redirects' => TRUE,
      'headers' => [
        'Authorization' => 'Token ' . $this->apiToken,
      ],
    ];
    $this->logger->debug('Request ' . $url);
    try {
      $response = \Drupal::httpClient()->get($url, $options);
      if ($response->getStatusCode() == 200) {
        return $response->getBody()->getContents();
      }
      else {
        \Drupal::messenger()->addError("Failed to download file from $url (" . $response->getStatusCode() . ')');
      }
    } catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return NULL;
    }
    return NULL;
  }

  /**
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function getEnrichmentsDataset(string $format = 'json', array $filters = ['page_size' => 100]): array {
    $datasets = [];
    $total = 0;
    do {
      $url = $this->apiUrl . '/enrichment-datasets';
      $data = $this->downloadContent($url);
      $data = json_decode($data);
      $total += count($data->results);
      foreach ($data->results as $expression) {
        $expression_object = Work::enrichmentsDatasetFromJson($expression, $this);
        if (isset($datasets[$expression->url])) {
          continue;
        }
        $datasets[$expression->url] = $expression_object;
      }
    } while ($data->next);
    $this->logger->debug(sprintf('Processed expressions: %d', $total));
    $this->logger->debug(sprintf('Total unique works: %d', count($datasets)));
    return $datasets;
  }

  /**
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function getEnrichments(string $endpoint, string $format = 'json', array $filters = ['page_size' => 100]): array {
    $filters['format'] = $format;
    $works = [];
    $page = 1;
    $total = 0;
    $filters['page'] = $page;
    $query = http_build_query($filters);
    $url = $endpoint . '?' . $query;
    $data = $this->downloadContent($url);
    $data = json_decode($data);
    $total += count($data->enrichments);
    foreach ($data->enrichments as $enrichment) {
      $term = NULL;
      $key = array_search($enrichment->taxonomy_topic, array_column($data->taxonomy->children, 'slug'));
      // @todo Find a better way to handle this.
      // check: {indigo_api_url}/api/v3/taxonomy-topics/{slug}
      if (!is_int($key)) {
        foreach ($data->taxonomy->children as $child) {
          $children = $child->children;
          $key = array_search($enrichment->taxonomy_topic, array_column($children, 'slug'));
          if (is_int($key)) {
            $term = $children[$key];
            break;
          }
        }
      }
      $term = $term ?? $data->taxonomy->children[$key];
      $enrichment->term = $term;
      $enrichment->term->parent = (object) [
        'name' => $data->taxonomy->name,
        'slug' => $data->taxonomy->slug,
        'id' => $data->taxonomy->id,
      ];
      $enrichment_object = Enrichment::fromJson($enrichment, $this);
      $works[$enrichment->work][$enrichment_object->id] = $enrichment_object;
    }
    $this->logger->debug(sprintf('Processed expressions: %d', $total));
    $this->logger->debug(sprintf('Total unique works: %d', count($works)));
    return $works;
  }

}
