<?php

namespace Drupal\unep_legislation_indigo\Client;

class Enrichment {

  private string $work;

  private IndigoInterface $client;

  public string $taxonomy_topic;
  public string $provision_id;
  public int $id;

  /**
   * @throws \Exception
   */
  public static function fromJson($object, IndigoInterface $client): Enrichment {
    $work = new static();
    $work->setClient($client);
    $attributes = [
      'id' => 'id',
      'provision_id' => 'provision_id',
      'taxonomy_topic' => 'taxonomy_topic',
      'term' => 'term',
      'work' => 'work',
    ];
    foreach($attributes as $local => $remote) {
      if (isset($object->$remote)) {
        $work->$local = $object->$remote;
      } else {
        $work->$local = NULL;
      }
    }
    return $work;
  }

  /**
   * @return string
   */
  public function getTaxonomyTopic(): string {
    return $this->taxonomy_topic;
  }

  /**
   * @return string
   */
  public function getProvisionId(): string {
    return $this->provision_id;
  }

  /**
   * @return string
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getWork(): string {
    return $this->work;
  }

  public function getClient(): IndigoInterface {
    return $this->client;
  }

  public function setClient(IndigoInterface $client) {
    $this->client = $client;
  }
}
