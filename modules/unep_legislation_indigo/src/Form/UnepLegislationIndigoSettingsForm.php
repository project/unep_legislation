<?php

namespace Drupal\unep_legislation_indigo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for configuring UNEP Legislation Indigo settings.
 */
class UnepLegislationIndigoSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unep_legislation_indigo_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'unep_legislation_indigo.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unep_legislation_indigo.settings');

    $form['indigo_partner'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Indigo Partner'),
      '#default_value' => $config->get('indigo_partner'),
      '#description' => $this->t('PARTNER is the domain name of your site.'),
    ];
    $form['indigo_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Indigo API URL'),
      '#default_value' => $config->get('indigo_api_url'),
      '#description' => $this->t('Indigo platform API URL. Do not use trailing slash, e.g. <a target="_blank" href="https://edit.laws.africa/api/v3">https://edit.laws.africa/api/v3</a>'),
    ];
    $form['indigo_api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Indigo API token'),
      '#default_value' => $config->get('indigo_api_token'),
      '#description' => $this->t('Visit your Indigo account page to get the API token.'),
    ];

    // Add more form elements as needed.

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('unep_legislation_indigo.settings');
    $config->set('indigo_partner', $form_state->getValue('indigo_partner'));
    $config->set('indigo_api_token', $form_state->getValue('indigo_api_token'));
    $config->set('indigo_api_url', $form_state->getValue('indigo_api_url'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
