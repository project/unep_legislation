<?php

namespace Drupal\unep_legislation_indigo\Drupal;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation_indigo\Client\IndigoInterface;
use Drupal\unep_legislation_indigo\Client\Work;
use Psr\Log\LoggerInterface;

class DrupalImport {

  protected EntityTypeManagerInterface $entityTypeManager;
  protected LanguageManagerInterface $languageManager;
  protected LoggerInterface $logger;
  protected FileSystemInterface $fileSystem;
  protected FileUsageInterface $fileUsage;
  protected ModuleHandlerInterface $moduleHandler;
  protected string $defaultLangCode;
  public function __construct(EntityTypeManagerInterface $entityTypeManager,
          LanguageManagerInterface $languageManager,
          LoggerInterface $logger, FilesystemInterface $fileSystem,
          FileUsageInterface $fileUsage, ModuleHandlerInterface $moduleHandler) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $languageManager;
    $this->logger = $logger;
    $this->fileSystem = $fileSystem;
    $this->fileUsage = $fileUsage;
    $this->moduleHandler = $moduleHandler;
    $this->defaultLangCode = $this->languageManager->getDefaultLanguage()->getId();
  }

  /**
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
   */
  public function importWork(Work $work): ?NodeInterface {
    $frbr_uri = $work->getFrbrUri();
    $pointsInTime = $work->getExpressions();
    $node = $this->findNodeByWorkFrbrUri($frbr_uri);
    // If there is no node, create a new one with the oldest point in time.
    if (empty($node)) {
      $firstPointInTime = reset($pointsInTime);
      $revision = $this->prepareRevision($firstPointInTime);
      $first = reset($revision);
      $all_languages = array_keys($revision);
      $language = reset($all_languages);
      $this->logger->debug(sprintf('Creating new node for work in language %s: %s', $language, $frbr_uri));
      $node = $this->createNodeFromWork($first, $language);
      $node->set('field_is_stub', $work->getStub());
    }
    else {
      $this->logger->debug(sprintf('Found existing node %d for work %s: ', $node->id(), $frbr_uri));
    }
    foreach ($pointsInTime as $date => $pointInTime) {
      $translations = $this->prepareRevision($pointInTime);
      $this->createOrUpdateRevision($node, $translations, $date);
    }

    // @todo Remove all other obsolete points in time
    return $node;
  }

  /**
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
   */
  public function importEnrichments(Work $work, array $enrichments, string $field_json_destination, string $field_tag_entity_destination = '') {
    $frbr_uri = $work->getFrbrUri();
    $node = $this->findNodeByWorkFrbrUri($frbr_uri);
    if (empty($node)) {
      throw new \Exception(sprintf('Cannot find node for work %s', $frbr_uri));
    }
    $paragraph_tags = $tids = [];
    foreach ($enrichments as $enrichment) {
      if (!isset($paragraph_tags[$enrichment->provision_id])) {
        $paragraph_tags[$enrichment->provision_id] = [];
      }
      $this->moduleHandler->invokeAll('preprocess_indigo_terms', [$work, &$enrichment]);
      $term = $enrichment->term;
      $tid = $term->id;
      $root_tid = $term->parent->id;
      $tids[$tid] = ['target_id' => $tid];
      $tids[$root_tid] = ['target_id' => $root_tid];
      $paragraph_tags[$enrichment->provision_id][] = [
        'tid' => $tid,
        'labels' => [
          'en' => $term->name,
        ],
        'root' => [
          'tid' => $root_tid,
          'labels' => [
            'en' => $term->parent->name,
          ],
        ],
      ];
    }
    if (!$node->hasField($field_json_destination)) {
      throw new \Exception(sprintf('Cannot find field %s.', $field_json_destination));
    }
    if ($field_tag_entity_destination && !$node->hasField($field_tag_entity_destination)) {
      throw new \Exception(sprintf('Cannot find field %s.', $field_tag_entity_destination));
    }
    $this->sortParagraphsIds($node, $paragraph_tags);
    $node->set($field_json_destination, json_encode($paragraph_tags));
    if (!empty($field_tag_entity_destination)) {
      $node->set($field_tag_entity_destination, $tids);
    }
    $node->save();
  }

  /**
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  protected function createOrUpdateRevision(NodeInterface $node, array $translations, $pointInTimeDate) {
    if (!$revision = $this->getNodeRevisionByDate($node, $pointInTimeDate)) {
      // Create new revision in default language.
      $node->setNewRevision();
      $node->set('field_legislation_date', $pointInTimeDate . 'T00:00:00');
      $node->save();
      $revision = $this->getNodeRevisionByDate($node, $pointInTimeDate);
    }
    /**
     * @var string $langcode
     * @var \Drupal\unep_legislation_indigo\Client\Work $expression
     */
    foreach ($translations as $langcode => $expression) {
      $client = $expression->getClient();
      $translation = $revision->hasTranslation($langcode) ? $revision->getTranslation($langcode) : $revision->addTranslation($langcode);
      $translation->setTitle($expression->title);
      $translation->set('field_legislation_date', $pointInTimeDate . 'T00:00:00');
      $translation->set('field_frbr_uri', $expression->expression_frbr_uri);
      $translation->set('field_is_principal', (bool) $expression->principal);
      $translation->set('field_work_title', $expression->work_title);
      $translation->set('field_akn_status', $expression->repeal != NULL ? 'repealed' : NULL);
      $translation->set('field_database_source', $expression->sourceName);
      $translation->set('field_akn_numbered_title', $expression->numbered_title);
      $translation->set('field_parent_work', isset($expression->parent_work, $expression->parent_work->frbr_uri) ? $expression->parent_work->frbr_uri : NULL);
      $publicationDocument = $expression->getPublicationDocument();
      if (!empty($publicationDocument->url)) {
        $pdf_url = $publicationDocument->url;
        $extension = pathinfo($pdf_url, PATHINFO_EXTENSION);
        $filename = $publicationDocument->filename ?? ImportUtil::createFilenameFromFrBrUri($pdf_url, $extension);
        $pdf = $this->downloadFileEntity($pdf_url, $filename, $node->id(), $client, 'field_files');
        $translation->set('field_files', [$pdf]);
      } else {
        $this->logger->notice(
          sprintf('No PDF file found for revision %s:%s (nid: %d, vid: %d) for node %s', $pointInTimeDate, $langcode, $revision->id(), $revision->getRevisionId(), $node->id())
        );
      }
      $workDocument = $expression->getFilePdfUrl();
      if (!empty($workDocument)) {
        $extension = pathinfo($workDocument, PATHINFO_EXTENSION);
        $filename = ImportUtil::createFilenameFromFrBrUri($workDocument, $extension);
        $pdf = $this->downloadFileEntity($workDocument, $filename, $node->id(), $client, 'field_work_files_version');
        $translation->set('field_work_files_version', [$pdf]);
      }
      // @todo 22 Apr 2024: Fetch TOC, timeline
      if ($expression->getTocUrl() && $toc = $client->downloadContent($expression->getTocUrl())) {
        $translation->set('field_toc_json', $toc);
      } else {
        $this->logger->warning(
          sprintf('No TOC found for revision %s:%s (nid: %d, vid: %d) for node %s', $pointInTimeDate, $langcode, $revision->id(), $revision->getRevisionId(), $node->id())
        );
      }
      if ($expression->getTimelineUrl() && $timeline = $client->downloadContent($expression->getTimelineUrl())) {
        $translation->set('field_timeline_json', $timeline);
      } else {
        $this->logger->warning(
          sprintf('No TIMELINE found for revision %s:%s (nid: %d, vid: %d) for node %s', $pointInTimeDate, $langcode, $revision->id(), $revision->getRevisionId(), $node->id())
        );
      }
      if ($expression->getMarkupUrl() && $markup = $client->downloadContent($expression->getMarkupUrl())) {
        $markup = html_entity_decode($markup, ENT_QUOTES, 'UTF-8');
        $translation->set('field_markup', $markup);
      } else {
        $this->logger->warning(
          sprintf('No MARKUP found for revision %s:%s (nid: %d, vid: %d) for node %s', $pointInTimeDate, $langcode, $revision->id(), $revision->getRevisionId(), $node->id())
        );
      }
      $translation->set('field_is_stub', $expression->getStub());
      if ($expression->commencing_work) {
        $translation->set('field_commencing_work', json_encode($expression->commencing_work));
      }
      if ($expression->work_amendments) {
        $translation->set('field_work_amendments', json_encode($expression->work_amendments));
      }
      if ($translation->hasField('field_publication_date') && $expression->publication_date) {
        $translation->set('field_publication_date', $expression->publication_date . 'T00:00:00');
      }
      if ($translation->hasField('field_commencement_date') && $expression->commencement_date) {
        $translation->set('field_commencement_date', $expression->commencement_date . 'T00:00:00');
      }
      if ($translation->hasField('field_assent_date') && $expression->assent_date) {
        $translation->set('field_assent_date', $expression->assent_date . 'T00:00:00');
      }
      if ($translation->hasField('field_legislation_year') && $expression->getYear()) {
        $translation->set('field_legislation_year', $expression->getYear() . '-01-01');
      }
      $this->moduleHandler->invokeAll('preprocess_indigo_node', [&$translation, $expression]);
      // @todo Fix validation
      if ($translation->validate()) {
        $translation->save();
        $this->logger->info(
          sprintf('Updated revision %s:%s (nid: %d, vid: %d) for node %s', $pointInTimeDate, $langcode, $revision->id(), $revision->getRevisionId(), $node->id())
        );
      }
    }
  }

  /**
   * Handle language mappings expression for a work (depending on available
   * languages).
   *
   * @param $pointInTime
   *
   * @return array
   */
  public function prepareRevision($pointInTime): array {
    $default = \Drupal::languageManager()->getDefaultLanguage()->getId();
    $languages = array_keys(\Drupal::languageManager()->getLanguages());
    $ret = [];
    foreach ($languages as $langCode) {
      if (isset($pointInTime[$langCode])) {
        $ret[$langCode] = $pointInTime[$langCode];
      }
      else {
        $this->logger->debug('No expression for language: ' . $langCode);
      }
    }
    $ignored = [];
    foreach ($pointInTime as $langCode => $expression) {
      if (!isset($ret[$langCode])) {
        $this->logger->debug('Ignored language: ' . $langCode);
        $ignored[] = $langCode;
      }
    }
    // ['jp'] => ['en']
    // ['fr', 'jp'] => ['fr', 'en']
    if (empty($ret[$default]) && !empty($ignored)) {
      $first = reset($ignored);
      if ($first) {
        $this->logger->debug(sprintf('Mapping %s to default language %s', $default, $first));
        $ret[$default] = $pointInTime[$first];
      }
    }
    if (empty($ret[$default])) {
      $this->logger->warning(sprintf('No entry for default default language %s', $default));
    }
    uksort($ret, [$this, 'sortLanguagesDefaultFirst']);
    return $ret;
  }

  /**
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  public function createNodeFromWork(Work $work, $language): ?NodeInterface {
    $bundle = 'akn_legislation';
    $node = Node::create([
      'langcode' => $language,
      'type' => $bundle,
      'title' => $work->title,
      'field_work_frbr_uri' => $work->getFrbrUri(),
      'field_frbr_uri' => $work->expression_frbr_uri,
      'field_legislation_date' => $work->expression_date . 'T00:00:00',
      'field_countries' => ImportUtil::getCountryByName($work->getCountry()),
      'field_is_principal' => (bool) $work->principal,
      'field_work_title' => $work->work_title,
      'field_akn_status' => $work->repeal != NULL ? 'repealed' : NULL,
      'field_database_source' => $work->sourceName,
      'field_akn_numbered_title' => $work->numbered_title,
      'field_parent_work' => isset($work->parent_work, $work->parent_work->frbr_uri) ? $work->parent_work->frbr_uri : NULL,
    ]);
    if ($node->hasField('field_publication_date') && $work->publication_date) {
      $node->set('field_publication_date', $work->publication_date . 'T00:00:00');
    }
    if ($node->hasField('field_commencement_date') && $work->commencement_date) {
      $node->set('field_commencement_date', $work->commencement_date . 'T00:00:00');
    }
    if ($node->hasField('field_assent_date') && $work->assent_date) {
      $node->set('field_assent_date', $work->assent_date . 'T00:00:00');
    }
    if ($node->hasField('field_legislation_year') && $work->getYear()) {
      $node->set('field_legislation_year', $work->getYear() . '-01-01');
    }
    $this->moduleHandler->invokeAll('preprocess_indigo_node', [&$node, $work]);
    if ($node = $this->validateNodeSave($node)) {
      $node->save();
    }
    return $node;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNodeRevisionByDate($node, $pointInTimeDate): ?NodeInterface {
    $entityType = 'node';
    $bundle = 'akn_legislation';
    $date = $pointInTimeDate;
    $query = $this->entityTypeManager->getStorage($entityType)->getQuery()->allRevisions();
    $query->accessCheck();
    $query->condition('type', $bundle);
    $query->condition('field_legislation_date', $date . 'T00:00:00');
    // $query->condition('langcode', $this->defaultLangCode);
    $query->condition('nid', $node->id());
    // $sql = print_r((string)$query, TRUE);
    $entity_ids = $query->execute();
    $revisions = $this->entityTypeManager->getStorage($entityType)
      ->loadMultipleRevisions(array_keys($entity_ids));
    if (!empty($revisions)) {
      return reset($revisions);
    }
    return NULL;
  }

  /**
   *
   */
  protected function findNodeByWorkFrbrUri(?string $field_work_frbr_uri): ?EntityInterface {
    $node = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['field_work_frbr_uri' => $field_work_frbr_uri]);
    if ($node) {
      return reset($node);
    }
    return NULL;
  }

  /**
   * @throws \Exception
   */
  private function validateNodeSave(Node $node): ?NodeInterface {
    $validations = $node->validate();
    if ($validations->count() == 0) {
      return $node;
    }
    $msg = [];
    $fields = $validations->getFieldNames();
    foreach ($fields as $i => $field) {
      $msg[] = $field . ': ' . $validations->get($i)->getMessage();
    }
    throw new \Exception(sprintf(
      'Cannot create new node for work %s: (%s)',
      $node->get('field_work_frbr_uri')->value,
      implode(', ', $msg)
    ));
  }

  /**
   * Example:
   *   uksort($point, ['Drupal\unep_legislation_indigo\Drupal\DrupalImport',
   * 'sortLanguagesDefaultFirst']);.
   *
   * @param string $a
   * @param string $b
   *
   * @return int|string
   */
  public function sortLanguagesDefaultFirst(string $a, string $b) {
    $default = \Drupal::languageManager()->getDefaultLanguage()->getId();
    $order = [$default];
    $languages = array_keys(\Drupal::languageManager()->getLanguages());
    foreach ($languages as $langCode) {
      if (!isset($order[$langCode])) {
        $order[] = $langCode;
      }
    }

    $index_a = array_search($a, $order);
    $index_b = array_search($b, $order);

    if ($index_a === FALSE && $index_b === FALSE) {
      return strcasecmp($a, $b);
    }
    elseif ($index_a === FALSE) {
      return 1;
    }
    elseif ($index_b === FALSE) {
      return -1;
    }

    return $index_a - $index_b;
  }

  /**
   *
   */
  public function getFieldStoragePath($entity, $bundle, $field): ?string {
    $scheme = NULL;
    $path = NULL;
    if ($field_base = FieldStorageConfig::load("$entity.$field")) {
      $scheme = $field_base->getSetting('uri_scheme');
    }
    try {
      if ($field_instance = \Drupal::entityTypeManager()
        ->getStorage('field_config')
        ->load("$entity.$bundle.$field")) {
        $path = $field_instance->getSetting('file_directory');
      }
    }
    catch (\Exception $e) {
      $this->logger->error("Cannot load field instance: $entity.$bundle.$field - " . $e->getMessage());
    }
    if ($scheme && $path) {
      return $scheme . '://' . $path;
    }
    return NULL;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadManagedFileByUri($uri): ?File {
    $file = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $uri]);
    if ($file) {
      return reset($file);
    }
    return NULL;
  }

  /**
   * @throws \Exception
   */
  public function downloadFileEntity($url, $filename, $nid, IndigoInterface $indigo, $field, $force = FALSE,): ?File {
    $bundle = 'akn_legislation';
    $storage_path = $this->getFieldStoragePath('node', $bundle, $field);
    if (empty($storage_path)) {
      $this->logger->error('Cannot find field storage path for node.legislation.' . $field);
      return NULL;
    }
    if (empty($filename)) {
      $this->logger->error('Cannot create filename from FRBR URI: ' . $url);
      return NULL;
    }
    $destination = $storage_path . '/' . $filename;
    if (!file_exists($destination) || $force) {
      $content = $indigo->downloadContent($url);
      if (empty($content)) {
        return NULL;
      }
      $this->fileSystem->prepareDirectory($storage_path, FileSystemInterface::CREATE_DIRECTORY);
      $this->fileSystem->saveData($content, $destination, FileSystemInterface::EXISTS_REPLACE);
    }
    if (!($file = $this->loadManagedFileByUri($destination))) {
      $file = File::create([
        'filename' => basename($filename),
        'uri' => $destination,
        'status' => 1,
        'uid' => 0,
      ]);
      $file->save();
      $this->fileUsage->add($file, 'unep_legislation_indigo', 'node', $nid);
    }
    return $file;
  }

  /**
   * Sort tags.
   *
   * Sometimes terms are not returned in correct order from API.
   *
   * @param $node
   *   The entity object.
   * @param $paragraph_tags
   *   Array with ids.
   */
  public function sortParagraphsIds($node, &$paragraph_tags) {
    $dom = new \DOMDocument();
    $markup = $node->get('field_markup')->value;
    @$dom->loadHTML($markup);
    $xpath = new \DOMXPath($dom);
    $query = sprintf("//*[@id]");
    $list = $xpath->query($query);
    $idsOrder = [];
    foreach ($list as $value) {
      $idsOrder[] = $value->getAttribute('id');
    }
    if ($idsOrder) {
      uksort($paragraph_tags, function ($a, $b) use ($idsOrder) {
        return ((array_search($a, $idsOrder) > array_search($b, $idsOrder)) ? 1 : -1);
      });
    }
  }

}
