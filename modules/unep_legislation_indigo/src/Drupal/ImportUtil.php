<?php

namespace Drupal\unep_legislation_indigo\Drupal;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation_indigo\Client\IndigoInterface;
use Drupal\unep_legislation_indigo\Client\Work;
use Symfony\Component\String\Slugger\AsciiSlugger;

class ImportUtil {
  private static array $indigo2DrupalLangCode = [
    'ara' => 'ar',
    'eng' => 'en',
    'fra' => 'fr',
    'rus' => 'ru',
    'spa' => 'es',
    'zho' => 'zh-hans',

    'bul' => 'bg',
    'cat' => 'ca',
    'dan' => 'da',
    'deu' => 'de',
    'ell' => 'el',
    'hun' => 'hu',
    'isl' => 'is',
    'jpn' => 'ja',
    'kor' => 'ko',
    'nld' => 'nl',
    'por' => 'pt',
    'sqi' => 'sq',
    'srp' => 'sr',
    'swe' => 'sv',
    'nor' => 'nn',
    'swa' => 'sz',
    'afr' => 'af',
    'zul' => 'zu',
    'xho' => 'xh',
  ];

  /**
   * @throws \Exception
   */
  public static function getLanguageCode($code): ?string {
    if (isset(self::$indigo2DrupalLangCode[$code])) {
      return self::$indigo2DrupalLangCode[$code];
    } else {
      throw new \Exception(sprintf('Failed to decode language: %s', $code));
    }
  }


  public static function getCountryByName($name, $storage = 'taxonomy_term'): ?\Drupal\Core\Entity\EntityInterface {
    if (empty($name)) {
      return NULL;
    }
    $mapping = [
      'great britain' => 'United Kingdom of Great Britain and Northern Ireland',
      'united kingdom' => 'United Kingdom of Great Britain and Northern Ireland',
    ];
    $country_name = strtolower($name);
    $country_name = $mapping[$country_name] ?? $country_name;

    $query = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', 'countries')->accessCheck(FALSE);
    $or = $query->orConditionGroup()
      ->condition('name', $country_name, '=' )
      ->condition('field_iso2', $country_name, '=')
      ->condition('field_iso3', $country_name, '=');
    $query->condition($or);
    $results = $query->execute();
    if (!empty($results)) {
      return \Drupal::entityTypeManager()->getStorage($storage)->load(reset($results));
    }
    return NULL;
  }

  public static function createFilenameFromFrBrUri($uri, $extension): ?string {
    if (preg_match('/\/akn\//', $uri)) {
      $uri = preg_replace('/.*\/akn\/[a-z\-0-9]*/i', '', $uri);
    }
    if (preg_match("/\.$extension$/i", $uri)) {
      $uri = preg_replace("/\.$extension$/i", '', $uri);
    }
    $slugger = new AsciiSlugger();
    $filename = $slugger->slug($uri);
    return !empty($extension) ? $filename . '.' . $extension : $filename;
  }

  public static function formatLegislationNodeLink(NodeInterface $revision) {
    if ($revision->isLatestRevision()) {
      return Url::fromRoute('entity.node.canonical', ['node' => $revision->id()])->toString();
    } else {
      return Url::fromRoute('entity.node.revision', ['node' => $revision->id(), 'node_revision' => $revision->getRevisionId()])->toString();
    }
  }
}
