<?php

namespace Drupal\unep_legislation_indigo\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\unep_legislation_indigo\Client\IndigoV3;
use Drupal\unep_legislation_indigo\Drupal\DrupalImport;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - https://git.drupalcode.org/project/examples/-/tree/8.x-1.x/drush
 */
class UnepLegislationIndigoCommands extends DrushCommands {

  /**
   * The Drupal Indigo importer.
   *
   * @var \Drupal\unep_legislation_indigo\Drupal\DrupalImport
   */
  protected $indigoImporter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new UnepLegislationIndigoCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\unep_legislation_indigo\Drupal\DrupalImport $indigoImporter
   *   The Drupal Indigo importer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, DrupalImport $indigoImporter, ModuleHandlerInterface $moduleHandler) {
    parent::__construct();
    $this->entityTypeManager = $entityTypeManager;
    $this->indigoImporter = $indigoImporter;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Get the Indigo client for a provider.
   *
   * @param string $providerId
   *   The provider id.
   * @return IndigoV3
   *   The Indigo client.
   */
  protected function getClientByProvider(string $providerId) {
    $provider = $this->entityTypeManager->getStorage('indigo_data_provider')->load($providerId);
    $url = $provider->get('api_url');
    $token = $provider->get('api_key');
    return new IndigoV3($url, $token, $this->logger(), $provider->id());
  }

  /**
   * Get the provider website.
   *
   * @param string $providerId
   *   The provider id.
   * @return string
   *   The website.
   */
  protected function getProviderUri(string $providerId) {
    $provider = $this->entityTypeManager->getStorage('indigo_data_provider')->load($providerId);
    return $provider->get('url') ?? '';
  }
  /**
   * List works from the remote API.
   *
   * @command indigo:works
   * @description List works from the remote API.
   * @usage indigo:works
   *
   * @field-labels
   *    idx: #
   *    uri: FRBR URI
   *    poi: Points in time
   *    expr: Expressions
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function works($provider_id = 'indigo'): RowsOfFields {
    $client = $this->getClientByProvider($provider_id);
    $works = $client->getWorks();
    $ret = [];
    $i = 1;
    $totalExpressions = 0;
    foreach ($works as $frbr_uri => $work) {
      $poi = $work->getExpressions();
      $le = 0;
      foreach ($poi as $langs) {
        $totalExpressions += count($langs);
        $le += count($langs);
      }
      $ret[] = [
        'idx' => $i++,
        'uri' => $this->getProviderUri($provider_id) . '/works' . $frbr_uri,
        'poi' => count($poi),
        'expr' => $le,
      ];
    }
    $this->logger()->success(sprintf('Total works: %d', count($works)));
    $this->logger()->success(sprintf('Total expressions: %d', $totalExpressions));
    return new RowsOfFields($ret);
  }

  /**
   * List dataset's enrichments from the remote API.
   *
   * @command indigo:enrichments
   * @description List works from the remote API.
   * @usage indigo:enrichments
   *
   * @field-labels
   *    idx: #
   *    uri: URI
   *    enrich: Enrichments
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function enrichments($uri = '', $provider_id = 'indigo'): RowsOfFields {
    if (empty($uri)) {
      throw new \Exception('Enrichments URI is required. Please use drush indigo:enrichments-dataset command first.');
    }
    $client = $this->getClientByProvider($provider_id);
    $enrichments = $client->getEnrichments($uri);
    $ret = [];
    $i = 1;
    $totalEnrichments = 0;
    foreach ($enrichments as $endpoint => $enrichment) {
      $totalEnrichments += count($enrichment);
      $ret[] = [
        'idx' => $i++,
        'uri' => $this->getProviderUri($provider_id) . '/works' . $endpoint,
        'enrich' => count($enrichment),
      ];
    }
    $this->logger()->success(sprintf('Total works: %d', count($enrichments)));
    $this->logger()->success(sprintf('Total enrichments: %d', $totalEnrichments));
    return new RowsOfFields($ret);
  }

  /**
   * List enrichments dataset from the remote API.
   *
   * @command indigo:enrichments-dataset
   * @description List enrichments dataset from the remote API.
   * @usage indigo:enrichments-dataset
   *
   * @field-labels
   *    idx: #
   *    uri: URI
   *    endpoint: Endpoint
   *    name: Dataset
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function enrichmentsDataset($provider_id = 'indigo'): RowsOfFields {
    $client = $this->getClientByProvider($provider_id);
    $datasets = $client->getEnrichmentsDataset();
    $ret = [];
    $i = 1;
    foreach ($datasets as $dataset) {
      $ret[] = [
        'idx' => $i++,
        'uri' => $this->getProviderUri($provider_id) . '/enrichments/' . $dataset->id,
        'endpoint' => $dataset->url,
        'name' => $dataset->name,
      ];
    }
    $this->logger()->success(sprintf('Total works: %d', count($datasets)));
    return new RowsOfFields($ret);
  }

  /**
   * List single work from the remote API.
   *
   * @command indigo:work
   * @description List single work from the remote API.
   * @field-labels
   *     key: Attribute
   *     value: Value
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   A list of greetings.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function work($frbr_uri = '', $provider_id = 'indigo'): RowsOfFields {
    $client = $this->getClientByProvider($provider_id);
    $work = $client->getWork($frbr_uri);
    $pointsInTime = $work->getExpressions();
    $ret = [
      ['key' => 'URI', 'value' => $work->getFrbrUri()],
      ['key' => 'Title', 'value' => $work->getTitle()],
      ['key' => 'Created at', 'value' => $work->getCreatedAt()],
      ['key' => 'Updated at', 'value' => $work->getUpdatedAt()],
      ['key' => 'Country', 'value' => $work->getCountry()],
      ['key' => 'Points in time', 'value' => count($pointsInTime)],
    ];
    $c = 0;
    foreach ($pointsInTime as $dates) {
      $c += count($dates);
    }
    $ret[] = ['key' => 'Expressions', 'value' => $c];
    foreach ($pointsInTime as $date => $langs) {
      foreach ($langs as $language => $expr) {
        $ret[] = ['key' => '', 'value' => sprintf('- %s | %s | %s', $date, $language, $expr->expression_frbr_uri)];
      }
    }
    return new RowsOfFields($ret);
  }

  /**
   * List single work from the remote API.
   *
   * @command indigo:expression
   * @description List single work from the remote API.
   * @field-labels
   *     key: Attribute
   *     value: Value
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   A list of greetings.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function expression($frbr_uri = '', $provider_id = 'indigo'): RowsOfFields {
    $client = $this->getClientByProvider($provider_id);
    $work = $client->getExpression($frbr_uri);
    $ret = [
      ['key' => 'URI', 'value' => $work->getFrbrUri()],
      ['key' => 'Title', 'value' => $work->getTitle()],
      ['key' => 'Created at', 'value' => $work->getCreatedAt()],
      ['key' => 'Updated at', 'value' => $work->getUpdatedAt()],
      ['key' => 'Country', 'value' => $work->getCountry()],
    ];
    return new RowsOfFields($ret);
  }

  /**
   * Import a work from the remote API.
   *
   * @command indigo:import
   * @description Create or update work from the remote Indigo API.
   * @usage indigo:import /akn/in/act/directive/2016/320e indigo
   *
   * @param string $frbr_uri
   *   Work's FRBR URI.
   * @param string $provider_id
   *   Indigo data provider ID (indigo/africa etc.).
   *
   * @return void
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function importWork(string $frbr_uri, string $provider_id = 'indigo') {
    $client = $this->getClientByProvider($provider_id);
    $work = $client->getWork($frbr_uri);
    $this->indigoImporter->importWork($work);
  }

  /**
   * Import enrichments from the remote API.
   *
   * @command indigo:import-enrichments
   * @description Retrieve and attach enrichments to imported works.
   *
   * @param string $frbr_uri
   *   Work's FRBR URI.
   * @param string $provider_id
   *   Indigo data provider ID (indigo/africa etc.).
   * @param string $field_json_destination
   *   The Drupal field where the resulting JSON is saved.
   * @param string $field_tag_entity_destination
   *   Optional drupal field where the resulting term entities are stored.
   *
   * @example drush indigo:import-enrichments
   *   /akn/un-unep-minamata/act/convention/2013/mercury field_pp_goals_tags
   *   <info>Tag</info> enrichments for a legislation, extract markup from
   *   field_markup, store tags in field_pp_goals_tags.
   */
  public function importEnrichments(string $frbr_uri, string $provider_id, string $field_json_destination, string $field_tag_entity_destination = '') {
    $client = $this->getClientByProvider($provider_id);
    $work = $client->getWork($frbr_uri);
    $datasets = $client->getEnrichmentsDataset();
    foreach ($datasets as $endpoint => $dataset) {
      $datasetList = $client->getEnrichments($endpoint);
      if (!empty($datasetList[$frbr_uri])) {
        $this->indigoImporter->importEnrichments($work, $datasetList[$frbr_uri], $field_json_destination, $field_tag_entity_destination);
        return;
      }
    }
  }

  /**
   * Import a list of work expressions for a taxonomy topic.
   *
   * @see https://informea.indigo.ipbes.net/api/v3/schema/redoc#tag/taxonomy-topics/operation/taxonomy_topics_work_expressions_list
   *
   * @command indigo:import-expressions
   * @description Create or update work from the remote Indigo API.
   * @usage indigo:import-expressions /taxonomy-topics/website-leap-climate-change/work-expressions indigo
   *
   * @param string $uri
   *   Work expressions URI.
   * @param string $provider_id
   *   Indigo data provider ID (indigo/africa etc.).
   *
   * @return void
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function importWorkExpressions(string $uri, string $provider_id = 'indigo') {
    $client = $this->getClientByProvider($provider_id);
    try {
      $regex = '/\/taxonomy-topics\/(?<slug>(?:.)*)\/work-expressions?.*/';
      preg_match($regex, $uri, $matches);
      $slug = $matches['slug'] ?? NULL;
      $works = $client->getLegislations($uri, $slug);
      $this->moduleHandler->invokeAll('preprocess_work_expressions_list', [&$works, $slug]);
      $this->logger()->notice('Importing ' . count($works) . ' works from ' . $provider_id);
      foreach ($works as &$work) {
        $work = $client->getWork($work->frbr_uri);
        if (!$work) {
          continue;
        }
        $work->addSlugToTaxonomyTopicsList($slug);
        $this->indigoImporter->importWork($work);
      }
    }
    catch (\Exception $e) {
      $this->logger()->error('There was an error during the import' . $e);
    }
  }

}
