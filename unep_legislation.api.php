<?php

/**
 * @file
 * Hooks and documentation related to the UNEP Indigo.
 */

/**
 * Set fields used for "Alignments and linkages" block.
 *
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
function hook_alignments_and_linkages_fields(&$fields, $configuration) {
  $fields = [
    'field_pp_climate_change',
    'field_pp_goals',
  ];
}

/**
 * Sort tags.
 *
 * @param array $tags
 *   Multi-Dimensional array with tags.
 */
function hook_alignments_and_linkages_sort_tags(&$tags, $configuration) {
}

/**
 * Alter the tabs.
 *
 * @param $tabs
 *   Array with tabs.
 * @param \Drupal\node\NodeInterface $entity
 *   The node being rendered.
 */
function hook_unep_legislation_tabs(&$tabs, $entity) {
  $tabs['entity.node.canonical'] = 'View document';
}

/**
 * Import updates from indigo.
 *
 * @param $uris
 *   The uris to import from
 * @param $provider_name
 *   The provider id.
 */
function hook_taxonomy_topics_work_expressions_list(&$uris, $provider_name) {

}

/**
 * Preprocess the list of works.
 *
 * @param array $works
 *   List of works.
 * @param string $slug
 *   The slug of the taxonomy topic, if exists.
 */
function hook_preprocess_work_expressions_list(array &$works, string $slug = NULL) {
  // For example: exclude works related to a specific country.
}

/**
 * Check if legislation has enrichments. If FALSE, do not show checkbox.
 *
 * @param $node
 *   The node to check.
 * @param $has_enrichments
 *   TRUE if legislation has enrichments, FALSE otherwise. Defaults to TRUE for
 *   backward compatibility.
 */
function hook_preprocess_node_has_enrichments($node, &$has_enrichments) {
  // Query the project's enrichments field.
}
