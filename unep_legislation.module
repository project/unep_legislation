<?php

/**
 * @file
 * Basic module file for UNEP Legislation module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hook_theme().
 */
function unep_legislation_theme($existing, $type, $theme, $path) {
  return [
    'unep_legislation_help_page' => [
      'template' => 'unep-legislation-help-page',
      'variables' => [
        'tabs' => [],
      ],
    ],
    'legislation_latest_version_block' => [
      'template' => 'legislation-latest-version-block',
      'variables' => [
        'first_expression_date' => '',
        'second_expression_date' => '',
        'info' => [],
        'latest_revision' => NULL,
        'links' => [],
      ],
    ],
    'legislation_item_list' => [
      'template' => 'legislation-item-list',
      'variables' => [
        'date' => [],
        'title' => '',
        'revision' => [],
        'relation' => [],
        'file' => [],
        'events' => [],
        'latest_revision' => FALSE,
        'current_viewing' => FALSE,
      ],
    ],
    'legislation_reader_settings' => [
      'template' => 'legislation-reader-settings',
      'variables' => [
        'show_tags' => TRUE,
        'show_provisions' => FALSE,
        'show_inline_diff' => FALSE,
        'options' => [
          'highlight' => TRUE,
        ],
      ],
    ],
    'alignments_and_linkages' => [
      'template' => 'alignments-and-linkages',
      'variables' => [
        'form' => [],
      ],
    ],
    'history_tab_list' => [
      'template' => 'history-tab-list',
      'variables' => [
        'items' => [],
        'node_title' => null,
        'node_language' => null,
        'node_url' => null
      ],
    ],
    'diff_compare_to' => [
      'template' => 'diff-compare-to',
      'variables' => [
        'title' => '',
        'id' => '',
        'items' => [],
      ],
    ],
    'legislation_select' => [
      'template' => 'legislation-select',
      'variables' => [
        'wrapper_id' => '',
        'active_item' => '',
        'items' => [],
      ]
      ],
    'legislation_download' => [
      'template' => 'legislation-download',
      'variables' => [
        'wrapper_id' => '',
        'active_item' => '',
        'items' => [],
        'active_item_url' => '',
      ]
    ],
    'legislation_related_documents' => [
      'template' => 'legislation-related-documents',
      'variables' => [
        'amends' => [],
        'amended_by' => [],
        'repeals' => [],
        'repealed_by' => [],
        'commenced_by' => [],
        'subsidiary_legislation' => [],
      ],
    ],
    'legislation_share_button_block' => [
      'template' => 'legislation-share-button-block',
      'variables' => []
    ]
  ];
}

/**
 * Implements hook_help().
 */
function unep_legislation_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.unep_legislation':
      $defaultTabs = \Drupal::service('unep_legislation.utils')->getDefaultTabs();
      unset($defaultTabs['entity.node.canonical']);
      $defaultTabs = array_keys($defaultTabs);
      return [
        '#theme' => 'unep_legislation_help_page',
        '#tabs' => $defaultTabs,
      ];
  }
}

/**
 * Implements hook_entity_field_access().
 */
function unep_legislation_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
  $routeMatch = \Drupal::routeMatch();
  /** @var \Drupal\unep_legislation\UnepUtils $unepUtils */
  $unepUtils = \Drupal::service('unep_legislation.utils');
  $entity = $routeMatch->getParameter('node');
  if ($operation != 'view' || !$entity instanceof EntityInterface
    || !$unepUtils->hasAknUri($entity)) {
    return AccessResult::neutral();
  }
  $settings = $unepUtils->getAknNodeSettings($entity);
  // Check fields access only for node root.
  if (!in_array($routeMatch->getRouteName(), [
    'entity.node.revision',
    'entity.node.canonical',
  ])) {
    return AccessResult::neutral();
  }
  switch ($items->getName()) {
    case 'field_toc_json':
      if ($settings && isset($settings['akn_toc']) && !$settings['akn_toc']) {
        return AccessResult::forbidden();
      }
      break;
    case 'field_timeline_json':
      $hasAmendments = $unepUtils->legislationHasAmendments($items->getEntity());
      $showTimeline = ($settings && isset($settings['akn_timeline']) && $settings['akn_timeline']);
      // If there are no amendments but user wants to show timeline.
      if (!$showTimeline && !$hasAmendments) {
        return AccessResult::forbidden();
      }
      break;
  }
  return AccessResult::neutral();
}


/**
 * Implements hook_cron().
 */
function unep_legislation_cron() {
  $interval = 24 * 60 * 60;
  $state = \Drupal::state();
  $last_run = $state->get("unep_legislation.last_update", 0);
  if (empty($last_run) || (time() - $last_run) >= $interval) {
    \Drupal::service('unep_legislation.cron_manager')->runCron();
    $state->set('unep_legislation.last_update', \Drupal::time()->getRequestTime());
  }
}
