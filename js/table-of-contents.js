(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.initialLoadToC = {
    attach: function (context, settings) {
      // Load ToC expanded.
      $(once('tocBtnExpand', '.akoma-ntoso-toc', context)).each(function (event) {
        [...this.getElementsByTagName('details')].forEach(function (item, index) {
          if (!item.open) {
            item.setAttribute('open', 'true');
          }
        });

        const urlParams = new URLSearchParams(window.location.search);
        let highlights = urlParams.get('highlights');
        if (highlights) {
          highlights = highlights.split(',');
          highlights.forEach(highlight => {
            const element = document.getElementById(highlight);
            if (element) {
              $(element).addClass('highlighted');

              let tocLink = $('.akoma-ntoso-toc a[href="#' + highlight + '"]');
              tocLink.addClass('highlighted');

              scrollToHighlightedItem();
            }
          });
        }

        if (window.location.hash) {
          let hash = window.location.hash;
          $('.akoma-ntoso-toc [href="' + hash + '"]').addClass('selected')
        }
        // Let the user clear the search only if the input is not empty.
        var input = document.getElementById('filters--toc--filter');
        var tocSearchBtnReset = document.getElementById('tocSearchBtnReset');
        $(once('inputKeyup', input, context))
          .on('keyup', function (event) {
            clearInput(input, tocSearchBtnReset);
          })
          .each(function (event) {
            clearInput(input, tocSearchBtnReset);
          });
      })
    }
  }

  Drupal.behaviors.searchInTableOfContents = {
    attach: function (context, settings) {
      var input = document.getElementById('filters--toc--filter');
      $(once('search', input, context)).on('keyup', function (event) {
        var text = this.value;
        [...document.querySelectorAll('.akoma-ntoso-toc li')].reverse().forEach(function(li) {
          if (text === '') {
            li.style['display'] = '';
          } else if (li.textContent.toLowerCase().match(text.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1').toLowerCase())) {
            li.style['display'] = '';
            $(li).removeClass('excluded');
          } else {
            li.style['display'] = 'none';
            $(li).addClass('excluded');
            // Sub-list with all elements excluded.
            let detailTag = li.querySelector('details summary a');
            if (detailTag && !li.querySelector('details div ul li:not(.excluded)')) {
              // @todo Here we need to check if parent is a match and if true we should show element (and children).
              return;
            }
            if ($(li).closest('details').find('> summary > a').text().toLowerCase().match(text.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1').toLowerCase())) {
              li.style['display'] = '';
              $(li).removeClass('excluded');
            }
          }
        });
      });
    }
  };

  Drupal.behaviors.resetSearchInTableOfContents = {
    attach: function (context, settings) {
      var input = document.getElementById('filters--toc--filter');
      let tocSearchBtnReset = document.getElementById('tocSearchBtnReset');
      $(once('resetSearchInTableOfContents', tocSearchBtnReset, context)).on('click', function (event) {
        input.value = '';
        input.dispatchEvent(new KeyboardEvent('keyup', {'key': ''}));
        // Expand ToC when user clears the search.
        $('.akoma-ntoso-toc').each(function () {
          [...this.getElementsByTagName('details')].forEach(function (item, index) {
            if (!item.open) {
              item.setAttribute('open', 'true');
            }
          });
        });
      });
    }
  };

  Drupal.behaviors.collapseTableOfContents = {
    attach: function (context, settings) {
      let tocBtnExpand = document.getElementById('tocBtnExpand');
      let tocBtnCollapse = document.getElementById('tocBtnCollapse');
      $(once('tocBtnExpand', tocBtnExpand, context)).on('click', function (event) {
        $('.akoma-ntoso-toc').each(function () {
          [...this.getElementsByTagName('details')].forEach(function (item, index) {
            if (!item.open) {
              item.setAttribute('open', 'true');
            }
          });
        });
      });
      $(once('tocBtnCollapse', tocBtnCollapse, context)).on('click', function (event) {
        $('.akoma-ntoso-toc').each(function () {
          [...this.getElementsByTagName('details')].forEach(function (item, index) {
            if (item.open) {
              item.removeAttribute('open');
            }
          });
        });
      });
    }
  }

  Drupal.behaviors.highlightedSection = {
    attach: function (context, settings) {
      $(once('highlightedEffect', '.akoma-ntoso-toc a.akoma-ntoso-item-link', context)).on('click', function (event) {
        event.preventDefault();

        let $clickedLink = $(this);
        let id = $clickedLink.attr('href');
        let anchorId = id.substring(1);

        let anchor = $('#' + anchorId);
        sessionStorage.setItem('scroll_to', anchorId);
        if (!anchor.length) {
          anchor = $('[id^="' + anchorId + '"]').first();
        }

        let normalizedAnchor = anchor.attr('id') || anchorId;

        // Clear previous selections
        $('.akoma-ntoso .selected').removeClass('selected');
        $('.akoma-ntoso-toc a.active').removeClass('active');
        $('.akoma-ntoso-terms li.active').removeClass('active');
        $('.akoma-ntoso-body .active').removeClass('active');

        $(this).addClass('active');

        if (anchor) {
          $(anchor).addClass('selected');
        }

        let tocItem = $('.akoma-ntoso-terms li[anchor="#' + normalizedAnchor + '"]')[0];
        if (tocItem) {
          $(tocItem).addClass('active');
          if (typeof highlightActiveCard !== 'undefined') {
            highlightActiveCard(tocItem);
          }
        }
        let offset = window.innerHeight / 2;
        $('html, body').animate({
          scrollTop: $(anchor).offset().top - offset,
        }, 500);

        if (history.replaceState) {
          history.replaceState(null, null, id);
        } else {
          window.location.hash = id;
        }
      });
    }
  };

  function scrollToHighlightedItem() {
    setTimeout(() => {
      const tocContainer = document.querySelector('.block--scrollspy');

      if (tocContainer != null) {
        const highlightedItem = tocContainer.querySelector('.akoma-ntoso-item-without-children.highlighted, .akoma-ntoso-item-link.highlighted');

        if (highlightedItem != null) {
          const containerRect = tocContainer.getBoundingClientRect();
          const itemRect = highlightedItem.getBoundingClientRect();
          const relativeTop = itemRect.top - containerRect.top + tocContainer.scrollTop;

          tocContainer.scrollTo({
            top: relativeTop - 430,
            behavior: 'smooth'
          });
        }
      }
    }, 500);
  }

  function clearInput(input, tocSearchBtnReset) {
    if (!input.value) {
      $(tocSearchBtnReset).attr('disabled', '');
    } else {
      tocSearchBtnReset.removeAttribute('disabled');
    }
  }

  window.scrollToHighlightedItem = scrollToHighlightedItem;

}(jQuery, Drupal, once));
