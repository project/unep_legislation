(function ($, Drupal, drupalSettings, once) {

  'use strict';

  const readerSettings = getReaderSettings();
  const akomantosoBody = $('.akoma-ntoso-body');
  let tempShift = 0;
  let hide_tags = !!(hideTags());

  /**
   * Highlight editorial remarks. Default TRUE.
   *
   * @type {{attach: Drupal.highlightEditorialRemarks.attach}}
   */
  Drupal.behaviors.highlightEditorialRemarks = {
    attach: function (context, settings) {
      $(once('highlightEditorialRemarks', '#show-highlight')).on('click', function(event){
        $('.akoma-ntoso-body').toggleClass('decorate-remarks');
      });
    }
  }

  Drupal.behaviors.readerHideTags = {
    attach: function (context, settings) {
      window.addEventListener('load',function () {
        if (!!hide_tags) {
          let tagToggle = document.getElementById('show-tags');
          if (tagToggle && tagToggle.checked) {
            [...document.querySelectorAll('#show-tags, [for="show-tags"]')].forEach(function (item) {
              $(item).addClass('visually-hidden');
            });
          }
        }
      }, { once: true });
    }
  }

  /**
   * Show provisions for this legislation.
   *
   * Default FALSE. Show Provisions are rendered in the sidebar, in same the
   * container with tags. When user displays provisions, the tags are hidden.
   *
   * Class '.akoma-ntoso-provision' should be present in the page.
   *
   * @type {{attach: Drupal.showProvisions.attach}}
   */
  Drupal.behaviors.showProvisions = {
    attach: function (context, settings) {
      if (!readerSettings) {
        return;
      }
      $(once('showProvisions', '#show-provisions')).on('click', function(event){
        let checkbox = document.getElementById('show-tags');
        if (checkbox) {
          checkbox.checked = !this.checked;
          // Hide terms when showing provisions.
          if (!hide_tags) {
            $('.akoma-ntoso-terms .pp-terms-list').toggleClass('visually-hidden');
          }
        }
        $('.akoma-ntoso-provision .pp-provisions-list').toggleClass('visually-hidden');
        processHighlights([], '');
        if (this.checked && !document.querySelector('.pp-provisions-list')) {
          loadProvisions();
        }
      });
      $(once('hideProvisions', '#show-tags')).on('click', function(event){
        resetProvisionContainer();
        let checkbox = document.getElementById('show-provisions');
        if (checkbox) {
          checkbox.checked = !this.checked;
          // Display terms when hide provisions.
        }
        $('.akoma-ntoso-terms .pp-terms-list').toggleClass('visually-hidden');
        $('.akoma-ntoso-provision .pp-provisions-list').toggleClass('visually-hidden');
      });
      document.addEventListener('click',function (event) {
        if (!$(event.target).hasClass('btn-see-provision')) {
          return;
        }
        resetProvisionContainer()
        let id = $(event.target).parent().attr('anchor').replace('#', '');
        highlightActiveProvision($(event.target).parent()[0]);
        getDiffSets(id);
      });
    }
  }

  window.resetProvisionContainer = function resetProvisionContainer() {
    let provisionElement = document.getElementsByClassName('reader-provision-changes-inline');
    if (provisionElement.length !== 0) {
      let oldProvisionId = $(provisionElement[0]).attr('anchor').replace('#', '');
      provisionElement[0].remove();
      $(document.getElementById(oldProvisionId)).removeClass('visually-hidden');
    }
  };
  /**
   * Load all provisions that have been amended.
   *
   * @returns {Promise<void>}
   */
  async function loadProvisions() {
    const url = `${serviceUrl()}/e/changed-provisions${frbrExpressionUri()}`;
    $.ajax({
      url: url,
      crossDomain: true,
    }).done(function(response) {
      let provisions = response.provisions;
      if (provisions && provisions.length) {
        decorateChangedProvisions(provisions);
      }
    });
  }
  /**
   * Calls the "diffsets" API, which provides the changes for that particular
   * eId as an HTML snippet.
   *
   * @param provisionId
   * @returns {Promise<void>}
   */
  async function getDiffSets(provisionId) {
    const url = `${serviceUrl()}/e/diffsets${frbrExpressionUri()}?id=${provisionId}`;
    const resp = await fetch(url);
    let data = null;
    if (resp.ok) {
      data = (await resp.json()).diffsets;
      data = data ? data[0] : null;
      resetProvisionContainer()
      let prev_expression_date = data['prev_expression_date'];
      let new_expression_date = data['new_expression_date'];
      let html =
        `<div class="reader-provision-changes-inline ig mb-3" anchor="#${provisionId}">
          <div class="card border-warning">
            <div class="card-header">
              <div class="d-flex mb-2 mb-lg-0">
                <div class="h5 flex-grow-1">${Drupal.t('What changed?')}</div>
                <button type="button" class="btn btn-secondary" id="close-reader-provision">${Drupal.t('Close')}</button>
              </div>
              <div class="row">
                <div class="col-12 col-lg-6">
                  <select class="form-control">
                    <option>Between ${prev_expression_date} and ${new_expression_date}</option>
                  </select>
                </div>
                <div class="col-6 d-none d-lg-block">
                  <label>
                    <input type="checkbox" id="show-provision-side-by-side" checked />${Drupal.t('Show changes side-by-side')}
                  </label>
                </div>
              </div>
            </div>
            <div class="card-body reader-provision-changes-inline-body">
              <div class="d-flex justify-content-between pa-3">
                <div class="diffset diffset-left">${data['html_diff']}</div>
                <div class="diffset diffset-right"> ${data['html_diff']}</div>
              </div>
            </div>
          </div>
        </div>`;
      let parentGuest = document.getElementById(provisionId);
      $(parentGuest).addClass('visually-hidden');
      parentGuest.parentNode.insertBefore($(html)[0], parentGuest.nextSibling);
      let $showProvisionSideBySide = document.getElementById('show-provision-side-by-side');
      $($showProvisionSideBySide).on('click', function(event) {
        $('.reader-provision-changes-inline .diffset-left').toggleClass('visually-hidden');
      });
      let $btnCloseProvision = document.getElementById('close-reader-provision');
      $($btnCloseProvision).on('click', function(event) {
        let provisionElement = $(event.target).parent().closest('.reader-provision-changes-inline');
        let id = provisionElement.attr('anchor').replace('#', '');
        provisionElement[0].remove();
        $(document.getElementById(id)).removeClass('visually-hidden');
        shiftItems(document.querySelectorAll('.pp-terms-list.pp-provisions-list.list-group li[anchor]'), - tempShift)

      })
    }
  }

  /**
   * Return HTML snippet for each provision.
   *
   * @param provisions
   */
  function decorateChangedProvisions(provisions = []) {
    let ol = document.createElement('ol');
    // @todo: Provisions should work/show and hide even without tags.
    // Some legislations do not have tags.
    $('.akoma-ntoso-provision').append(ol);
    ol.classList = 'pp-terms-list pp-provisions-list list-group';
    provisions.forEach(provision => {
      let li = document.createElement('li');
      li.setAttribute('anchor', '#' + provision.id);
      li.classList = 'list-group-item';
      li.innerHTML = `
        <p>${Drupal.t('This provision has been amended.')}</p>
        <button type="button" class="btn btn-see-provision">${Drupal.t('What changed?')}</button>`;
      ol.appendChild(li);

      calculateProvisionPosition(li);
    });
  }

  Drupal.behaviors.loadDiff = {
    attach: function (context, settings) {
      const cleanAknBody = $('.akoma-ntoso-body')[0].innerHTML;
      $(once('loadDiff', '.comparing-to-dropdown .dropdown-menu a', context)).on('click', async function (event) {
        event.preventDefault();
        // Make sure to remove all highlights.
        processHighlights([], '');
        $(event.target).parent().find('.visually-hidden').removeClass('visually-hidden');
        $(event.target).addClass('visually-hidden');
        // Show differences.
        let sidebyside = document.getElementById('show-side-by-side') ? document.getElementById('show-side-by-side').checked : false;
        loadDiff($(event.target).attr('href'), sidebyside);
        // Update btn label.
        let btn = $(event.target).parent().closest('.comparing-to-dropdown').find('.diff-compare-to-label');
        btn[0].textContent = Drupal.t('Compare to ') + event.target.textContent;
        btn.addClass('active');
        // Show legend.
        $(document.querySelector('.comparing-to-dropdown .legend')).removeClass('visually-hidden');
        // Hide other settings when "Compare to" is not active.
        [...document.querySelectorAll('#show-provisions, [for="show-provisions"], #show-tags, [for="show-tags"]')].forEach(function (item) {
          $(item).addClass('visually-hidden');
        });
        [...document.querySelectorAll('#show-side-by-side, [for="show-side-by-side"]')].forEach(function (item) {
          $(item).removeClass('visually-hidden');
        });
        $(document.getElementsByTagName('body')).addClass('akoma-ntoso-bigger-body')
      })
      $(once('resetDiff', '.btn-reset-diff-compare-to', context)).on('click', async function (event) {
        // Reset to original content.
        $('.akoma-ntoso-body')[0].innerHTML = cleanAknBody;
        // Reset btn label.
        let btn = $(event.target).parent().closest('.comparing-to-dropdown').find('.diff-compare-to-label');
        btn[0].textContent = Drupal.t('Compare to');
        btn.removeClass('active');
        // Reset dropdown elements.
        $(document.querySelector('.comparing-to-dropdown .dropdown-menu a.visually-hidden')).removeClass('visually-hidden');
        // Hide legend.
        $(document.querySelector('.comparing-to-dropdown .legend')).addClass('visually-hidden');
        // Show other settings when "Compare to" is not active.
        [...document.querySelectorAll('#show-provisions, [for="show-provisions"], #show-tags, [for="show-tags"]')].forEach(function (item) {
          $(item).removeClass('visually-hidden');
        });
        [...document.querySelectorAll('#show-side-by-side, [for="show-side-by-side"]')].forEach(function (item) {
          $(item).addClass('visually-hidden');
        });
        $(document.getElementsByTagName('body')).removeClass('akoma-ntoso-bigger-body');
      })
      $(once('showInline', '.show-side-by-side', context)).on('click', async function (event) {
        let uri = $('.comparing-to-dropdown .dropdown-menu a.visually-hidden').attr('href');
        loadDiff(uri, this.checked);
      })

      /**
       *
       * @param uri
       * @param sideBySide
       */
      function loadDiff(uri, sideBySide = false) {
        $.ajax({
          url: uri,
          beforeSend: function(){
            $('#loading-spinner').toggleClass('hidden');
            akomantosoBody.toggleClass('opacity-5');
          },
          success: function(){
            $('#loading-spinner').toggleClass('hidden');
            akomantosoBody.toggleClass('opacity-5');
          }
        }).done(function(response) {
          let data = response[0].data;
          getDocumentDiff(data, sideBySide, response[0].is_older);
        });
      }
    }
  }

  async function getDocumentDiff(data, sideBySide = false, isOlder = false) {
    let newAknBody = "";
    newAknBody = sideBySide ? `<div class="row ${!isOlder ? 'compare-with-older' : ''} d-flex justify-content-between pa-3"><div class="col-6 diffset diffset-left">`
      + data.html_diff + '</div><div class="col-6 diffset diffset-right">' + data.html_diff + "</div></div>" : `<div class="diffset ${!isOlder ? 'compare-with-older' : ''}">` + data.html_diff + `</div>`
    $('.akoma-ntoso-body')[0].innerHTML = newAknBody;
  }

  /**
   * Calculate current position for an element.
   *
   * @param item
   */
  function calculateProvisionPosition(item) {
    let id = item.getAttribute('anchor');
    if (!$(id).length) {
      item.classList.add('not-linked');
      return;
    }
    // Correct element position relative to the paragraph.
    let idPosition = $(id).offset().top - akomantosoBody.offset().top + akomantosoBody.position().top;
    // Actual position in concordance with the siblings.
    if (item.previousSibling) {
      let previousSiblingHeight = $(item.previousSibling).outerHeight();
      let previousSiblingPosition = $(item.previousSibling).offset().top - akomantosoBody.offset().top + akomantosoBody.position().top;
      // Check if the previous item is too close to the current one.
      if (idPosition < previousSiblingPosition + previousSiblingHeight + 30) {

        idPosition = previousSiblingPosition + previousSiblingHeight + 30;
      }
    }
    item.style['top'] = idPosition + 'px';
  }

  /**
   * Get base URL.
   *
   * @returns {`https://informea.indigo.ipbes.net/embeds/v1/p/${*|string}`}
   */
  const serviceUrl = () => {
    const partner = drupalSettings.indigo_partner ?? window.location.hostname;
    const baseUrl = drupalSettings.indigo_url ?? '';
    if (baseUrl === 'https://api.laws.africa') {
      return `https://services.lawsafrica.com/v1/p/${partner}`;
    }
    return `${baseUrl}/embeds/v1/p/${partner}`;
  };

  /**
   * Get reader settings.
   *
   * @returns {*}
   */
  function getReaderSettings() {
    return drupalSettings.reader_settings;
  }

  /**
   * Get FRBR expression URI.
   *
   * @returns {*}
   */
  function frbrExpressionUri() {
    return drupalSettings.reader_settings.frbr_uri;
  }

  /**
   * Highlight the selected item and move to the corresponding position.
   *
   * @param item
   */
  function highlightActiveProvision(item) {
    let id = $(item).attr('anchor');
    if (!$(id).length) {
      return;
    }

    // Correct item position relative to the paragraph.
    let idPosition = $(id).offset().top - akomantosoBody.offset().top + akomantosoBody.position().top;
    // Actual position in concordance with the siblings.
    let cardPosition = parseFloat(item.style.top.replace('px', ''));
    tempShift = cardPosition - idPosition;
    shiftItems(item.parentNode.querySelectorAll('li[anchor]'), tempShift);

  }
  function shiftItems(items, shiftPosition) {
    items.forEach(function (item) {
      let position = parseFloat(item.style['top'].replace('px', ''));
      item.style['top'] = (position - shiftPosition) + 'px';
    });
  }
}(jQuery, Drupal, drupalSettings, once));
