(function ($, Drupal) {

  Drupal.behaviors.aknBtnShareUrl = {
    attach: function (context, settings) {
      $(once('aknBtnShareUrl', '#akn-generate-url')).on('click', function () {
        let aknUrlArguments = {
          'highlights' : sessionStorage.getItem('highlights'),
          'text' : sessionStorage.getItem('text'),
          'hide_tags' : sessionStorage.getItem('hide_tags'),
        };
        let anchor = sessionStorage.getItem('scroll_to') ? '#'.concat(sessionStorage.getItem('scroll_to')) : '';
        let newURL = window.location.origin + window.location.pathname + '?' + objectToQueryString(aknUrlArguments) + anchor;
        $(document.getElementById('aknCollapseCopyStatus').querySelector('input')).attr('value', newURL);
      });
      $(once('legislative_guide', '#akn-btn-copy-link')).on('click', function (ev) {
        ev.preventDefault();
        $(document.getElementById('aknCollapseCopyStatus').querySelector('input')).select();
        //@todo: use an alternative
        document.execCommand("copy");
      }).hover(function () {
      }, function() {
        $('#aknCollapseCopyStatus').collapse('hide');
      });
    }
  }

}(jQuery, Drupal));
