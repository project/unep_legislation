(function ($, Drupal, drupalSettings, once) {

  'use strict';

  Drupal.behaviors.poolpartyTermsPosition = {
    attach: function (context) {
      $(once('termsTaggingPosition', '.akoma-ntoso-terms', context)).each(function () {
        this.querySelectorAll('li[anchor]').forEach(function (item) {
          calculateTermPosition(item);
        });
      });
      window.addEventListener('resize', function () {
        document.querySelectorAll('.akoma-ntoso-terms li[anchor]').forEach(function (item) {
          calculateTermPosition(item);
        });
      });
      window.addEventListener('load',function () {
        const urlParams = new URLSearchParams(window.location.search);
        let list = document.querySelector('ol.pp-terms-list.js-opacity-0');
        $(list).removeClass('js-opacity-0');
        let hide_tags = hideTags();
        if (!!hide_tags) {
          $('.akoma-ntoso-terms .pp-terms-list').addClass('visually-hidden');
        }
        if (drupalSettings.interactive) {
          document.addEventListener('mouseover',function (event) {
            if ($(event.target).hasClass('interactive-mode')) {
              $(event.target).on( "mouseover", function() {
                let cardHeight = $(event.target).outerHeight();
                if (cardHeight) {
                  let nextCard = $(event.target).next()[0];
                  let nextCardPosition = nextCard.style['top'].replace('px', '');
                  if (!nextCard.hasAttribute('correct-position')) {
                    nextCard.setAttribute('correct-position', nextCardPosition);
                  }
                  let cardPosition = parseFloat($(event.target)[0].style['top'].replace('px', ''));
                  let shiftPosition = -(cardPosition + cardHeight + 10 - nextCardPosition);
                  if (shiftPosition < 0) {
                    shiftItems([...$(event.target).next()], shiftPosition);
                  }
                  showLessTerms(event.target);
                }
              })
                .on( "mouseleave", function() {
                  this.querySelectorAll('li[anchor]').forEach(function (item) {
                    calculateTermPosition(item);
                  });
                });
            }
          });
        }
      }, { once: true });
    }
  };

  Drupal.behaviors.termHighlight = {
    attach: function (context) {
      $(once('termHighlight', '.akoma-ntoso-terms li[anchor]', context)).on('click', function () {
        highlightActiveCard(this);
      });
    }
  };

  Drupal.behaviors.paragraphHighlight = {
    attach: function (context) {
      $(once('paragraphHighlight', '.akoma-ntoso-body .enrichment[data-eid]', context)).on('click', function () {
        let clickedElement = $(this);
        let id = clickedElement.attr('data-eid');
        let fullAnchor = '#' + id;

        let anchor = $('[id^="' + id + '"]').first();
        let normalizedAnchor = '#' + (anchor.attr('id') || id);

        $('.akoma-ntoso .selected').removeClass('selected');
        $('.akoma-ntoso-toc a.active').removeClass('active');
        $('.akoma-ntoso-terms li.active').removeClass('active');
        $('.akoma-ntoso-body .active').removeClass('active');

        clickedElement.addClass('selected');

        let $tocLink = $('.akoma-ntoso-toc a[href="' + normalizedAnchor + '"]');
        $tocLink.addClass('selected');

        let tocItem = $('.akoma-ntoso-terms li[anchor="' + fullAnchor + '"]').first();
        if (tocItem.length) {
          tocItem.addClass('active');
          if (typeof highlightActiveCard === 'function') {
            highlightActiveCard(tocItem[0]);
          }
        }

        if (history.replaceState) {
          history.replaceState(null, null, fullAnchor);
        }
      });
    }
  };

  /**
   * Calculate current position for an element.
   *
   * @param item
   */
  function calculateTermPosition(item) {
    const akomantosoBody = $('.akoma-ntoso-body');
    let id = item.getAttribute('anchor');
    if (!$(id).length) {
      item.classList.add('not-linked');
      return;
    }
    // Correct element position relative to the paragraph.
    let idPosition = $(id).offset().top - akomantosoBody.offset().top + akomantosoBody.position().top;
    // Actual position in concordance with the siblings.
    if (item.previousSibling) {
      let previousSiblingHeight = $(item.previousSibling).outerHeight();
      let previousSiblingPosition = $(item.previousSibling).offset().top - akomantosoBody.offset().top + akomantosoBody.position().top;
      // Check if the previous item is too close to the current one.
      if (idPosition < previousSiblingPosition + previousSiblingHeight) {
        idPosition = previousSiblingPosition + previousSiblingHeight + 30;
      }
    }
    item.style['top'] = idPosition + 'px';
    // Mark paragraphs with terms.
    akomantosoBody.find(id).addClass('enrichment');
  }

  /**
   * Highlight the selected item and move to the corresponding position.
   *
   * @param item
   */
  function highlightActiveCard(item) {
    const akomantosoBody = $('.akoma-ntoso-body');
    let id = $(item).attr('anchor');
    if (!$(id).length) {
      return;
    }

    $('.akoma-ntoso .selected').removeClass('selected');
    $('.akoma-ntoso-toc a.active').removeClass('active');
    $('.akoma-ntoso-terms li.active').removeClass('active');
    akomantosoBody.find('.active').removeClass('active');

    let tocLink = $('.akoma-ntoso-toc a[href="' + id + '"]');
    if (tocLink.length === 0) {
      let anchor = $(id).first();
      let normalizedAnchor = '#' + (anchor.attr('id') || id.substring(1));
      tocLink = $('.akoma-ntoso-toc a[href="' + normalizedAnchor + '"]');
    }

    $(item).addClass('active');
    akomantosoBody.find(id).addClass('active');

    if (tocLink.length) {
      tocLink.addClass('active');
    }

    // Correct item position relative to the paragraph.
    let idPosition = $(id).offset().top - akomantosoBody.offset().top + akomantosoBody.position().top;
    // Actual position in concordance with the siblings.
    let cardPosition = parseFloat(item.style.top.replace('px', ''));
    let tempShift = cardPosition - idPosition;
    shiftItems(item.parentNode.querySelectorAll('li[anchor]'), tempShift);

    if (drupalSettings.interactive) {
      showLessTerms(item);
    }

    document.addEventListener('mousedown', function selector(e) {
      // Stop propagation if item or paragraph is already active.
      if (e.target.classList.contains('active') ||
        $(e.target).parent().closest('.enrichment.active').length > 0 ||
        $(e.target).parent().closest('.list-group-item.active').length > 0) {
        e.stopPropagation();
        return;
      }
      $('.akoma-ntoso-terms li[anchor]').removeClass('active');
      akomantosoBody.find('.active').removeClass('active');
      $('.akoma-ntoso-toc a.active').removeClass('active');
      if (drupalSettings.interactive) {
        showSeeMoreTerms(item);
      }
      shiftItems(item.parentNode.querySelectorAll('li[anchor]'), -tempShift);
    }, { once: true });
  }

  /**
   * Shift items in order to place the selected item at the correct position.
   *
   * @param items
   *   A list with items.
   * @param shiftPosition
   *   Shift position.
   */
  function shiftItems(items, shiftPosition) {
    items.forEach(function (item) {
      let position = parseFloat(item.style['top'].replace('px', ''));
      item.style['top'] = (position - shiftPosition) + 'px';
    });
  }

  /**
   * In interactive mode, when user interacts with the card, show all terms.
   *
   * @param card
   *   An element with terms.
   */
  function showSeeMoreTerms(card) {
    [...$(card).find('.pp-terms')[0].children].forEach(function (item, index) {
      if (index === 0 || $(item).hasClass('interactive-mode--see-more')) {
        return;
      }
      $(item).addClass('visually-hidden');
    });
  }

  /**
   * In interactive mode, when user stop to interact with the card, show less
   * terms.
   *
   * @param card
   *   An element with terms.
   */
  function showLessTerms(card) {
    [...$(card).find('.pp-terms')[0].children].forEach(function (item) {
      $(item).removeClass('visually-hidden');
    });
  }

  // Make highlightActiveCard globally accessible
  window.highlightActiveCard = highlightActiveCard;

}(jQuery, Drupal, drupalSettings, once));
