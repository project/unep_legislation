(function ($, Drupal) {

  let hide_tags = !!(hideTags());

  Drupal.AjaxCommands.prototype.aknLegislationTagFilter = function (ajax, response) {
    let highlights = response.highlights ? response.highlights.split(',') : [];
    let term_name = response.term ? response.term : '';
    let tagToggle = document.getElementById('show-provisions');
    if (tagToggle && tagToggle.checked) {
      tagToggle.click();
      let checkbox = document.getElementById('show-provisions');
      if (!checkbox) {
        checkbox.checked = !this.checked;
        // Display terms when hide provisions.
        if (!hide_tags) {
          $('.akoma-ntoso-terms .pp-terms-list').removeClass('visually-hidden');
        }
      }
      $('.akoma-ntoso-provision .pp-provisions-list').addClass('visually-hidden');
    }
    setSessionArguments(response.highlights, term_name, hideTags())
    sessionStorage.setItem('scroll_to', highlights[0]);
    processHighlights(highlights, term_name, true);
    let aknUrlArguments = {
      'highlights' : sessionStorage.getItem('highlights'),
      'text' : sessionStorage.getItem('text'),
      'hide_tags' : sessionStorage.getItem('hide_tags'),
    };
    let anchor = sessionStorage.getItem('scroll_to') ? '#'.concat(sessionStorage.getItem('scroll_to')) : '';
    let newURL = window.location.origin + window.location.pathname + '?' + objectToQueryString(aknUrlArguments) + anchor;
    window.history.replaceState({}, '', newURL);
  };

  function objectToQueryString(obj) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  window.objectToQueryString = objectToQueryString;

}(jQuery, Drupal));