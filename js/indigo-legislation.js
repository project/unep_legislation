(function ($, Drupal, drupalSettings, once) {

  'use strict';

  const disableTooltips = drupalSettings.disable_tooltips;

  /**
   * Behavior to handle highlighting based on URL parameters and hash.
   */
  Drupal.behaviors.initialLoadScroll = {
    attach: function (context, settings) {
      window.addEventListener('load', function () {
        // Get highlights from URL parameters
        const urlParams = new URLSearchParams(window.location.search);
        let highlights = urlParams.get('highlights') ? urlParams.get('highlights').split(',') : [];
        let text = urlParams.get('text');
        setSessionArguments(urlParams.get('highlights'), text, hideTags())
        // Highlights extra paragraphs.
        processHighlights(highlights, text, false);

        // Handle scrolling to hash
        if (window.location.hash) {
          let hash = window.location.hash.substring(1);
          hash = decodeURIComponent(hash);
          sessionStorage.setItem('scroll_to', hash);
          let anchor = document.getElementById(hash);
          if (anchor) {
            // Apply offset to scroll.
            let offset = window.innerHeight / 2;
            $('html, body').scrollTop($(anchor).offset().top - offset);
          }
        }
      });
    }
  };

  /**
   * AKN terms.
   *
   * AKN terms initially are <span> tags with [data-refersto] attribute. They
   * are transformed into anchors following indigo logic.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.aknTermsRefersTo = {
    attach: function (context, settings) {
      if (disableTooltips) {
        return;
      }
      $(once('aknTermsRefersTo', '.akoma-ntoso-body .akn-term[data-refersto]', context)).each(function (event) {
        // Transform <span> terms with attr [data-refersto] in anchors.
        var id = this.getAttribute('data-refersto');
        var anchor = document.createElement('span');
        [...this.attributes].forEach(attr => {
          anchor.setAttribute(attr.nodeName, attr.nodeValue)
        });
        // Follow Indigo logic and add #defn-id as href attribute.
        anchor.setAttribute('href', id);
        anchor.innerHTML = this.innerHTML;
        this.parentNode.replaceChild(anchor, this);
        // Follow Indigo logic: update id and add data-defines attribute to the
        // referenced paragraph.
        var refParagraph = $('.akoma-ntoso-body .akn-p[data-refersto="' + id + '"]');
        if (refParagraph) {
          refParagraph.attr('id', id.replace('#', ''));
          refParagraph.attr('data-defines', refParagraph.attr('data-refersto'));
          const innerText = this.innerText;
          let thisId = this.getAttribute('id');
          let tippyInitialized = false;
          $(anchor).on('mouseenter', () => {
            if(!tippyInitialized) {
              var divPopup = buildDecorateTermsPopup(refParagraph.html(), innerText);
              loadTippy('#' + thisId, divPopup.innerHTML);
              tippyInitialized = true;
            }
          })
        }
      });
    }
  }

  /**
   * AKN internal reference.
   *
   * Paragraphs referenced inline into other paragraphs.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.aknInternalRefs = {
    attach: function (context, settings) {
      if (disableTooltips) {
        return;
      }
      $(once('aknInternalRefs', '.akoma-ntoso-body .akn-ref[data-href]', context)).each(function (event) {
        var id = this.getAttribute('data-href').replace('#', '');
        var internalRefs = document.querySelector('.akoma-ntoso-body [data-eid="' + id + '"]');
        if (internalRefs) {
          let tippyInitialized = false;
          $(`.akoma-ntoso-body .akn-ref[data-href='#${id}']`).on('mouseenter', () => {
            if(!tippyInitialized) {
              var divPopup = buildDecorateTermsPopup(internalRefs.outerHTML);
              loadTippy('[data-href="' + this.getAttribute('data-href') + '"]', divPopup.innerHTML);
              tippyInitialized = true;
            }
          })
        }
      });
    }
  }

  Drupal.behaviors.highlightSectedTerm = {
    attach: function (context, settings) {
      $(once('highlightSelectedTerm', '.akoma-ntoso-body .akn-ref[data-href], .akoma-ntoso-body .akn-term[data-refersto], .akoma-ntoso-body a[href]', context)).on('click', function (event) {
        var id = $(this).attr('href');
        $(id).addClass('selected');
      });
    }
  };

  /**
   * Function to handle highlights, TOC links, and smooth scrolling.
   * @param {Array} highlights - List of highlight IDs.
   * @param {String} term - The term to wrap in a span (e.g., 'Target 10').
   * @param {Boolean} shouldScroll - Check if it should scroll to first element or scroll to anchor
   */
  function processHighlights(highlights, term = '', shouldScroll = false) {
    //todo: use drupalSettings in the future to set this value
    //Highlight term even if it's multiple terms are present
    let always_highlight = false;

    resetProvisionContainer();
    $('.highlighted').removeClass('highlighted');
    $('.akoma-ntoso-terms li.active').removeClass('active');
    $('.item-list--comma-list .list-group-item').removeClass('term-background');

    if (highlights && highlights.length > 0) {
      $('.akoma-ntoso-terms').addClass('filtering');
      highlights.forEach(function (highlight, index) {

        // Select the element with an ID that starts with the highlight
        let highlightedElement = $('[id^="' + highlight + '"]').first();

        if (highlightedElement.length) {
          $(highlightedElement).addClass('akoma-ntoso highlighted');
        } else {
          return;
        }

        // Get the actual ID of the element
        let actualId = highlightedElement.attr('id');

        // Find the matching TOC link by traversing up the DOM
        let tocLink = findMatchingTocLink(highlightedElement[0]);
        if (tocLink) {
          tocLink.classList.add('highlighted');
          scrollToHighlightedItem();
        }

        // Find the corresponding list item in the terms
        let listItem = $('.akoma-ntoso-terms li[anchor="#' + actualId + '"]').first();
        if (listItem.length) {
          listItem.addClass('highlighted active');
        }

        let items = listItem.find('.item-list--comma-list .list-group-item');
        let itemCount = items.length;
        items.each(function() {
          let text = $(this).text().trim();  // Only get the text inside the <li>
          if (text === term && (itemCount > 1 || always_highlight)) {
            $(this).addClass('term-background');
          }
        });

        // Scroll to the first highlighted element if shouldScroll is true and this is the first element
        if (index === 0 && shouldScroll && highlightedElement) {
          let offset = window.innerHeight / 2;
          $('html, body').animate({
            scrollTop: $(highlightedElement).offset().top - offset
          }, 500);
        }
      });
    } else {
      $('.akoma-ntoso-terms').removeClass('filtering');
      $('.akoma-ntoso-terms li[anchor]').each(function () {
        $(this).removeClass('active highlighted');
      });
    }
  }

  function setSessionArguments(highlights, text, hide_tags) {
    sessionStorage.setItem('highlights', highlights);
    sessionStorage.setItem('text', text);
    sessionStorage.setItem('hide_tags', hide_tags);
  }

  /**
   * Url parameter to hide tags
   *
   * @returns {number|number}
   */
  function hideTags() {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('hide_tags') ? parseInt(urlParams.get('hide_tags')) : 0;
  }

  /**
   * Finds the corresponding TOC link for a given element by traversing up the DOM.
   * Prioritizes data-anchor over id for matching.
   * @param {HTMLElement} element - The starting element to traverse from.
   * @returns {HTMLElement|null} - The matching TOC link or null if not found.
   */
  function findMatchingTocLink(element) {
    while (element && element !== document.body) {
      const dataAnchor = element.getAttribute('data-anchor');
      if (dataAnchor) {
        // Attempt to find the TOC link with data-anchor="dataAnchor"
        const tocLink = document.querySelector(`.akoma-ntoso-toc a[data-anchor="${dataAnchor}"]`);
        if (tocLink) {
          return tocLink;
        }
      }
      const id = element.id;
      if (id) {
        // Attempt to find the TOC link with href="#id"
        const tocLink = document.querySelector(`.akoma-ntoso-toc a[href="#${id}"]`);
        if (tocLink) {
          return tocLink;
        }
      }
      element = element.parentElement;
    }
    return null;
  }

  /**
   * Create a div element for tippy with the given information.
   *
   * @param body
   *   The markup.
   * @param title
   *   A title.
   * @returns {HTMLDivElement}
   */
  function buildDecorateTermsPopup(body = '', title = '') {
    var divPopup = document.createElement('div');
    divPopup.className = 'la-decorate-terms__popup tooltip';
    if (title) {
      var divTitle = document.createElement('div');
      divTitle.classList.add('tippy-content__title');
      divTitle.innerText = title;
      divPopup.appendChild(divTitle);
    }
    var divBody = document.createElement('div');
    divBody.classList = 'tippy-content__body akoma-ntoso';
    divBody.innerHTML = body;
    divPopup.appendChild(divBody);
    return divPopup;
  }

  /**
   * Call tippy function.
   *
   * @param target
   *   The target paragraph element.
   * @param content
   *   The content of the tooltip.
   */
  function loadTippy(target, content) {
    tippy(target, {
      content: content,
      allowHTML: true,
      interactive: true,
      maxWidth: 450,
      appendTo: document.body,
      theme: 'light-border',
    });
  }

  // Make processHighlights globally accessible
  window.processHighlights = processHighlights;

  window.hideTags = hideTags;

  window.setSessionArguments = setSessionArguments;

}(jQuery, Drupal, drupalSettings, once));
