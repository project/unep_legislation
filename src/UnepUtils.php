<?php

namespace Drupal\unep_legislation;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 * Utility class for UNEP legislation.
 */
class UnepUtils {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\RevisionableStorageInterface
   */
  protected $nodeStorage;

  /**
   * The UNEP Legislation settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new UnepUtils object.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * Check if an URI is in AKN format.
   *
   * @param string $uri
   *   Example: /akn/za/act/1993/31/eng@1993-01-01
   *
   * @return bool
   */
  public function isAknUri(string $uri) {
    return strpos($uri, '/akn/') === 0;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return bool
   *   TRUE if AKN URI exists and is valid, FALSE otherwise.
   */
  public function hasAknUri(EntityInterface $node) {
    return ($node->hasField('field_frbr_uri') && !$node->get('field_frbr_uri')->isEmpty());
  }

  /**
   * Get the AKN URI for a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return string
   *   Return the AKN URI.
   */
  public function getAknUri(EntityInterface $node) {
    return $node->get('field_frbr_uri')->value;
  }

  /**
   * Check is a legislation is a stub.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return boolean
   *   TRUE if the legislation is a stub, FALSE otherwise.
   */
  public function isStub(EntityInterface $node) {
    return $node->get('field_is_stub')->value ?? FALSE;
  }

  /**
   * Get AKN URI for a node.
   *
   * @param string $nid
   *   The node ID to be checked.
   * @param string $langcode
   *   The language code.
   *
   * @return string|null
   *   The AKN URI for the node if exists, otherwise NULL.
   */
  public function getLatestFrbrUriForNode(string $nid,  string $langcode) {
    $node = $this->nodeStorage->load($nid);
    if (!$node instanceof NodeInterface || $node->bundle() != 'akn_legislation') {
      return NULL;
    }

    if (!$this->hasAknUri($node) || !$this->isAknUri($node->field_frbr_uri->value)) {
      return NULL;
    }

    $node = $this->nodeStorage->load($node->id());
    if ($node->language()->getId() == $langcode) {
      return $node->field_frbr_uri->value;
    }

    if ($node->hasTranslation($langcode)) {
      $node = $node->getTranslation($langcode);
      return $node->field_frbr_uri->value;
    }

    return $node->field_frbr_uri->value ?? NULL;
  }

  /**
   * Get AKN URI for a revision.
   *
   * @param string $nid
   *   The node ID to be checked.
   * @param string $revisionId
   *   The revision ID to be checked.
   * @param string $langcode
   *   The language code.
   *
   * @return string|null
   *   The AKN URI for the revision if exists, otherwise NULL.
   */
  public function getRevisionFrbrUriForNode(string $nid, string $revisionId, string $langcode) {
    $node = $this->nodeStorage->load($nid);
    if (!$node instanceof NodeInterface || $node->bundle() != 'akn_legislation') {
      return NULL;
    }

    $revision = $this->nodeStorage->loadRevision($revisionId);
    if (!$this->hasAknUri($revision) || !$this->isAknUri($revision->field_frbr_uri->value)) {
      return NULL;
    }
    if ($revision->language()->getId() == $langcode) {
      return $revision->field_frbr_uri->value;
    }

    if ($revision->hasTranslation($langcode)) {
      $revision = $revision->getTranslation($langcode);
      return $revision->field_frbr_uri->value;
    }

    return NULL;
  }

  public function getRevisionFrbrUriFoDate(EntityInterface $node, $pointInTimeDate) {
    $date = $pointInTimeDate;
    $query = $this->nodeStorage->getQuery()
      ->allRevisions()
      ->accessCheck()
      ->condition('type', 'akn_legislation')
      ->condition('field_legislation_date', $date . 'T00:00:00')
      ->condition('nid', $node->id());
    $rids = $query->execute();
    $revisions = $this->nodeStorage->loadMultipleRevisions(array_keys($rids));
    if (empty($revisions)) {
      return NULL;
    }
    $revision = reset($revisions);
    return $revision->field_frbr_uri->value;
  }

  /**
   * Retrieve a revision with a frbr uri.
   *
   * @param string $uri
   *   Example: /akn/za/act/1993/31/eng@1993-01-01
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The specified entity revision or NULL if not found.
   */
  public function getRevisionFromFrbrUri(string $uri) {
    $uri = urldecode($uri);
    if (!$this->isAknUri($uri)) {
      return NULL;
    }

    $query = $this->database->select('node_revision__field_frbr_uri', 'n');
    $query->fields('n', ['entity_id', 'revision_id', 'langcode']);
    $query->condition('field_frbr_uri_value', $uri);
    $query->orderBy('revision_id', 'DESC');
    $result = $query->execute()->fetchAssoc();
    if (!empty($result['entity_id'])) {
      return $this->nodeStorage->loadRevision($result['revision_id'])->getTranslation($result['langcode']);
    }

    return NULL;
  }


  /**
   * Retrieve the latest revision from a Work FRBR URI.
   *
   * For example '/akn/za/act/1993/31' will return the latest revision and the
   * uri will be something like '/akn/za/act/1993/31/eng@1993-01-01'.
   *
   * @param string $uri
   *   Example: /akn/za/act/1993/31
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The specified entity revision or NULL if not found.
   */
  public function getLatestRevisionFromWorkFrbrUri(string $uri) {
    $uri = urldecode($uri);
    if (!$this->isAknUri($uri)) {
      return NULL;
    }

    $query = $this->database->select('node_revision__field_work_frbr_uri', 'n');
    $query->fields('n', ['entity_id', 'revision_id', 'langcode']);
    $query->condition('field_work_frbr_uri_value', $uri);
    $query->orderBy('revision_id', 'DESC');
    $query->range(0, 1);
    $result = $query->execute()->fetchAssoc();
    if (!empty($result['entity_id'])) {
      $latestId = $this->nodeStorage->getLatestRevisionId($result['entity_id']);
      return $this->nodeStorage->loadRevision($latestId)->getTranslation($result['langcode']);
    }

    return NULL;
  }

  /**
   * Get next revision date.
   *
   * @param $nid
   *   The node ID.
   * @param $date
   *   The current date.
   *
   * @return string|null
   *   The next revision date.
   */
  public function getNextRevisionDateFromDate($nid, $date) {
    $node = $this->nodeStorage->load($nid);
    if ($node->bundle() != 'akn_legislation' || !$this->hasAknUri($node)) {
      return NULL;
    }
    $data = json_decode($node->get('field_timeline_json')->value);
    if (!$data->timeline) {
      return NULL;
    }
    $values = array_column($data->timeline, 'date');
    $values = array_reverse($values);
    foreach ($values as $key => $value) {
      if ($value != $date) {
        continue;
      }
      $nextRevisionDate = array_slice($values, $key + 1, 1);
      return reset($nextRevisionDate);
    }

    return NULL;
  }

  /**
   * Transform terms from JSON format to array.
   *
   * @param string $value
   *   Value of the field in JSON format.
   * @param array $rootIds
   *   All parents ids found.
   * @param string $langcode
   *   The language code.
   *
   * @return array
   */
  public function formatTerms(string $value, array &$rootIds, string $langcode): array {
    $data = json_decode($value);
    if (empty($data)) {
      return [];
    }
    $list = [];
    foreach ($data as $terms) {
      foreach ($terms as $term) {
        if (empty($term->root)) {
          continue;
        }
        if (!in_array($term->root->tid, $rootIds)) {
          $rootIds[$term->root->tid] = !empty($term->root->labels->$langcode) ? $term->root->labels->$langcode : $term->root->labels->en;
        }
        $list[$term->root->tid][$term->tid] = !empty($term->labels->$langcode) ? $term->labels->$langcode : $term->labels->en;
      }
    }
    return $list;
  }

  /**
   * Get default tabs.
   *
   * @return array
   *   Array with default tabs.
   */
  public function getDefaultTabs() {
    return [
      'unep_legislation.overview' => t('Overview'),
      'entity.node.canonical' => t('Document'),
      'unep_legislation.history' => t('History'),
    ];
  }

  /**
   * Get the node settings.
   *
   * Each node can override the settings with a field field_akn_settings.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node.
   *
   * @return array
   *   Array with settings.
   */
  public function getAknNodeSettings(EntityInterface $node) {
    if (!$node->hasField('field_akn_settings')
      || $node->get('field_akn_settings')->isEmpty()) {
      return [];
    }
    $settings = [];
    $rows = preg_split('/\n|\r\n/', $node->get('field_akn_settings')->value);
    foreach ($rows as $row) {
      [$config, $values] = explode(':', $row);
      $settings[$config] = $values;
    }
    return $settings;
  }

  /**
   * Get a specific setting.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node.
   *
   * @param string $id
   *   The setting id.
   *
   * @return mixed|null
   *   The setting value or NULL if not found.
   */
  public function getAknNodeSetting(EntityInterface $node, string $id) {
    $settings = $this->getAknNodeSettings($node);
    return ((empty($settings[$id])) ? [] : explode(',', $settings[$id]));
  }

  public function hasTimelineJson(EntityInterface $node): bool {
    return ($node->hasField('field_timeline_json') && !$node->get('field_timeline_json')->isEmpty());
  }

  public function getTimelineJson(EntityInterface $node) {
    if (!$node->hasField('field_timeline_json')) {
      return [];
    }
    $data = json_decode($node->get('field_timeline_json')->value);
    if (empty($data->timeline)) {
      return [];
    }
    return $data->timeline;
  }

  /**
   * Check if a legislation has amendments.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *    The node.
   *
   * @return bool
   *   TRUE if legislation has amendments, FALSE otherwise.
   */
  public function legislationHasAmendments(EntityInterface $node) {
    return $this->legislationHasEventsByType($node, 'amendment');
  }

  /**
   * Check if a legislation has consolidations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *    The node.
   *
   * @return bool
   *   TRUE if legislation has consolidations, FALSE otherwise.
   */
  public function legislationHasConsolidations(EntityInterface $node) {
    return $this->legislationHasEventsByType($node, 'consolidation');
  }

  /**
   * Check if a legislation has a specific event type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *    The node.
   * @param  string $type
   *    The event type.
   *
   * @return bool
   *   TRUE if legislation has $type, FALSE otherwise.
   */
  public function legislationHasEventsByType(EntityInterface $node, string $type) {
    if (!$this->hasTimelineJson($node)) {
      return FALSE;
    }
    $value = $node->get('field_timeline_json')->value;
    $data = json_decode($value);
    if (empty($data->timeline)) {
      return FALSE;
    }
    $withType = FALSE;
    foreach ($data->timeline as $item) {
      if (!$item->events || !in_array($type, array_column($item->events, 'type'))) {
        continue;
      }
      $withType = TRUE;
      break;
    }

    return $withType;
  }

  /**
   * Return the number of appearance of a specific event type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *    The node.
   * @param string $type
   *    The event type.
   *
   * @return int|null
   *   Return the number of appearance.
   */
  public function eventNumberOfAppearance(EntityInterface $node, string $type) {
    if (!$this->hasTimelineJson($node)) {
      return FALSE;
    }
    $value = $node->get('field_timeline_json')->value;
    $data = json_decode($value);
    if (empty($data->timeline)) {
      return FALSE;
    }
    $events = array_column($data->timeline, 'events');
    $appearance = [];
    $appearance = array_merge_recursive($appearance, array_map(function($event) {
      $types = array_column($event, 'type');
      return reset($types);
    }, $events));
    $appearance = array_count_values($appearance);
    return $appearance[$type] ?? 0;
  }

  /**
   * Determines if the provided legislation has multiple consolidation events.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return bool
   */
  public function hasMultipleConsolidations(EntityInterface $node) {
    $appearance = $this->eventNumberOfAppearance($node, 'consolidation') ?? 0;
    return $appearance > 1;
  }

  /**
   * Find all appearances as amended by/related for a legislation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return array
   *   Return a list with legislations.
   */
  public function getEventAllAppearances(EntityInterface $node) {
    $work_frbr_uri = $node->get('field_work_frbr_uri')->value;
    $query = $this->nodeStorage->getQuery()
      ->accessCheck()
      ->condition('type', 'akn_legislation')
      ->condition('field_timeline_json', $work_frbr_uri, 'CONTAINS');
    $ids = $query->execute();
    return $this->nodeStorage->loadMultiple($ids);
  }

  /**
   * Get all amends of a legislation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return array
   */
  public function getAmends(EntityInterface $node) {
    $work_frbr_uri = $node->get('field_work_frbr_uri')->value;
    $query = $this->nodeStorage->getQuery()
      ->accessCheck()
      ->condition('type', 'akn_legislation')
      ->condition('field_timeline_json', $work_frbr_uri, 'CONTAINS');
    $ids = $query->execute();
    $nodes = $this->nodeStorage->loadMultiple($ids);
    $items = [];
    foreach ($nodes as $node) {
      $items[] = ['title' => $node->label(), 'url' => $node->toUrl()->toString()];
    }
    return $items;
  }

  /**
   * Get all legislations amended by a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return array
   */
  public function getLegislationAmendedBy(EntityInterface $node) {
    if (!$this->legislationHasAmendments($node)) {
      return [];
    }
    return $this->getLegislationEventsByType($node, 'amendment');
  }

  /**
   * Ger all legislation repeals by a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return array
   */
  public function getLegislationRepealedBy(EntityInterface $node) {
    if (!$this->legislationHasEventsByType($node, 'repeal')) {
      return [];
    }
    return $this->getLegislationEventsByType($node, 'repeal');
  }

  /**
   * Get all legislations amended/repealed/commenced by a legislation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return array
   */
  public function getLegislationEventsByType(EntityInterface $node, string $type) {
    $value = $node->get('field_timeline_json')->value;
    $data = json_decode($value);
    $items = [];
    foreach ($data->timeline as $item) {
      $item->events = array_filter($item->events, function($event) use ($type) {
        return $event->type == $type;
      });
      foreach ($item->events as &$event) {
        $by_frbr_uri = $event->by_frbr_uri;
        $entity = $this->getLatestRevisionFromWorkFrbrUri($by_frbr_uri);
        if (!$entity instanceof EntityInterface) {
          $event->by_frbr_uri = NULL;
        }
        $items[] = [
          'date' => $item->date,
          'event' => $event,
        ];
      }
    }
    return $items;
  }

  /**
   * Get all subsidiaries of a legislation.
   *
   * @param $frbr_uri
   *   The FRBR URI of the legislation.
   *
   * @return array
   */
  public function getSubsidiaryLegislation($frbr_uri) {
    $nodes = $this->nodeStorage->loadByProperties([
      'type' => 'akn_legislation',
      'field_parent_work' => $frbr_uri,
    ]);
    $result = [];
    foreach ($nodes as $node) {
      $result[] = ['title' => $node->label(), 'url' => $node->toUrl()->toString()];
    }
    return $result;
  }

  /**
   * Get the database provider for a node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node.
   *
   * @return \Drupal\indigo_data_provider\Entity\IndigoDataProvider|null
   */
  public function getNodeProvider(EntityInterface $node): mixed {
    if (!$node->hasField('field_database_source') || $node->get('field_database_source')->isEmpty()) {
      return NULL;
    }
    $database = $node->get('field_database_source')->value;
    return $this->entityTypeManager->getStorage('indigo_data_provider')->load($database);
  }

  /**
   * Get the highlighted paragraphs from a legislation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node.
   * @param string $field
   *   The field name.
   * @param array $arguments
   *   The array of filters.
   *
   * @return string|null
   *   A string list with highlighted prepared as argument for a URL.
   */
  public function getHightlightedParagraphs(EntityInterface $node, string $field, array $arguments) {
    // Load the JSON with relationships between paragraphs and climate tags.
    $tree = (array) json_decode($node->get($field)->value ?? '');
    // Preserve only paragraphs with filtered tags.
    $tree = array_filter($tree, function ($value) use ($arguments) {
      return !empty(array_intersect($arguments, array_column((array)$value, 'tid')));
    });
    $highlights = array_keys($tree);
    if (empty($highlights)) {
      return NULL;
    }
    return implode(',', $highlights);
  }

  /**
   * Determines if an entity has multiple points in time.
   *
   * @param EntityInterface $node
   *   The entity whose revisions are being checked.
   *
   * @return bool
   *   TRUE if the entity has more than one revision, FALSE otherwise.
   */
  public function hasMultiplePointsInTime(EntityInterface $node) {
    $rIds = $this->nodeStorage->revisionIds($node);
    return count($rIds) > 1;
  }

}
