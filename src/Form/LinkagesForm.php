<?php

namespace Drupal\unep_legislation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\unep_legislation\Ajax\TagFilterCommand;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for Alignments and Linkages.
 */
class LinkagesForm extends FormBase {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * The AKN Legislation node.
   *
   * @var null|Drupal\Core\Entity\EntityInterface
   */
  protected $node = NULL;

  /**
   * The entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(RouteMatchInterface $routeMatch, ModuleHandlerInterface $moduleHandler, UnepUtils $unepUtils, EntityTypeManagerInterface $entityTypeManager) {
    $this->routeMatch = $routeMatch;
    $this->moduleHandler = $moduleHandler;
    $this->unepUtils = $unepUtils;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('module_handler'),
      $container->get('unep_legislation.utils'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alignments_and_linkages_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $configuration = []) {
    $fields = explode(',', $configuration['block_fields']);
    $this->node = $this->routeMatch->getParameter('node');
    $configuration['entity'] = $this->node;
    $this->moduleHandler->invokeAll('alignments_and_linkages_fields', [&$fields, $configuration]);
    $vocabularies = [];
    $tags = $this->getTags($fields, $vocabularies);
    if (!$tags) {
      return [];
    }
    $form['fields'] = [
      '#type' => 'hidden',
      '#value' => implode(',', $fields),
    ];
    if (!$configuration['show_only_terms']) {
      $form['vocabularies'] = [
        '#type' => 'select',
        '#options' => $vocabularies,
        '#default_value' => array_key_first($vocabularies),
        '#ajax' => [
          'callback' => '::getTermsCallback',
          'disable-refocus' => TRUE,
          'event' => 'change',
          'wrapper' => 'linkages-tags',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Loading ...'),
          ],
        ],
      ];
    }
    $this->moduleHandler->invokeAll('alignments_and_linkages_sort_tags', [&$tags, $configuration]);
    $options = array_replace(self::getDefaultOptions(), $tags[array_key_first($vocabularies)]);
    if ($configuration['show_only_terms']) {
      $options = array_combine(array_values($vocabularies), array_values($tags));
      array_unshift($options, $this->t('Select'));
    }
    $form['tags'] = [
      '#type' => 'select',
      '#options' => $options,
      '#prefix' => '<div id="linkages-tags">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => '::submitHighlightsCallback',
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => 'alignments-and-linkages',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Loading ...'),
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#attributes' => [
        'class' => ['visually-hidden'],
      ],
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * @param array $fields
   *   Array with fields storing tags.
   * @param $vocabularies
   *   All parents found.
   *
   * @return array
   *   Array with tags group by parent.
   */
  protected function getTags(array $fields, &$vocabularies) {
    $tags = [];
    foreach ($fields as $field) {
      if ($this->node->get($field)->isEmpty()) {
        continue;
      }
      $data = $this->node->get($field)->value;
      $tags = array_replace($tags, $this->unepUtils->formatTerms($data, $vocabularies, 'en'));
    }
    return $tags;
  }

  /**
   *
   * @param array $options
   *   The options array to search through.
   * @param int|string $tag_id
   *   The tag ID to find.
   *
   * @return string|null
   *   The label of the tag if found, NULL otherwise.
   */
  protected function findTag(array $options, $tag_id) {
    foreach ($options as $key => $value) {
      if ($key == $tag_id) {
        return is_array($value) ? reset($value) : $value;
      }

      if (is_array($value)) {
        $found = $this->findTag($value, $tag_id);
        if ($found !== null) {
          return $found;
        }
      }
    }
    return null;
  }

  /**
   * Get the terms callback.
   *
   * This callback will populate the 2nd select with children related to first
   * selection.
   *
   * @param array $form
   *   The build form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|mixed
   */
  public function getTermsCallback(array &$form, FormStateInterface $form_state) {
    $vocabularyId = (int) $form_state->getValue('vocabularies');
    if (!$vocabularyId) {
      return $form['tags'];
    }
    $fields = explode(',', $form_state->getValue('fields'));
    if (!$fields) {
      $form['tags']['#attributes']['disabled'] = 'disabled';
      return $form['tags']['#options'] = self::getDefaultOptions();
    }
    unset($form['tags']['#attributes']['disabled']);
    $vocabularies = [$vocabularyId];
    $tags = $this->getTags($fields, $vocabularies);
    $this->moduleHandler->invokeAll('alignments_and_linkages_sort_tags', [&$tags]);
    $form['tags']['#options'] = self::getDefaultOptions();
    $form['tags']['#options'] += $tags[$vocabularyId];

    return $form['tags'];
  }

  /**
   * Submit form.
   *
   * This callback will highlight paragraphs with the term selected.
   *
   * @param array $form
   *   The build form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response that highlights paragraphs with selected term.
   */
  public function submitHighlightsCallback(array &$form, FormStateInterface $form_state) {
    $tag = (int) $form_state->getValue('tags');
    $fields = explode(',', $form_state->getValue('fields'));
    if (!$fields) {
      $form['tags']['#attributes']['disabled'] = 'disabled';
      return $form['tags']['#options'] = self::getDefaultOptions();
    }
    $arguments = array_filter([$tag]);
    $termName = $this->findTag($form['tags']['#options'], $tag);
    $highlights = '';
    foreach ($fields as $field) {
      $fieldHighlights = $this->unepUtils->getHightlightedParagraphs($this->node, $field, $arguments);
      if (!$fieldHighlights) {
        continue;
      }
      $highlights .= $fieldHighlights;
    }
    $response = new AjaxResponse();
    $response->addCommand(new TagFilterCommand($highlights, $termName));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  static function getDefaultOptions() {
    return ['All' => t('Select Goal / Target')];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
