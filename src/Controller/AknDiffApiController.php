<?php

namespace Drupal\unep_legislation\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\unep_legislation\Ajax\LoadDiffCommand;
use Drupal\unep_legislation_indigo\Client\IndigoV3;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a controller for diff API.
 */
class AknDiffApiController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * A config object for the system performance configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new AknDiffApiController.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, EntityTypeManagerInterface $entityTypeManager) {
    $this->config = $config_factory->get('unep_legislation_indigo.settings');
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory')->get('unep_legislation'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Get the Indigo client for a provider.
   *
   * @param string $providerId
   *   The provider id.
   * @return IndigoV3
   *   The Indigo client.
   */
  protected function getClientByProvider(string $providerId) {
    $provider = $this->entityTypeManager->getStorage('indigo_data_provider')->load($providerId);
    $url = $provider->get('api_url');
    $token = $provider->get('api_key');
    $this->client = new IndigoV3($url, $token, $this->logger, $provider->id());
  }

  /**
   * Load the diff response.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function diff(EntityInterface $node, Request $request) {
    return $this->loadDiffResponse($request, $node, NULL);
  }

  /**
   *  Load the diff response for a revision.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *    The node entity.
   * @param \Drupal\Core\Entity\EntityInterface $node_revision
   *    The node revision.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *    The incoming request.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function revisionDiff(EntityInterface $node, EntityInterface $node_revision, Request $request) {
    return $this->loadDiffResponse($request, $node, $node_revision);
  }

  /**
   * Load the diff response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request.
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node entity.
   * @param \Drupal\Core\Entity\EntityInterface|null $node_revision
   *   The node revision.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function loadDiffResponse(Request $request, EntityInterface $node, EntityInterface $node_revision = NULL) {
    $database = $node->get('field_database_source')->value;
    $this->getClientByProvider($database);
    $query = $request->query->all();
    $query['query']['format'] = 'json';
    $url = $this->entityTypeManager->getStorage('indigo_data_provider')->load($database)->get('api_url');
    $frbrUri = ($node_revision) ? $node_revision->get('field_frbr_uri')->value : $node->field_frbr_uri->value;
    $url = sprintf('%s%s/diff?expression-frbr-uri=%s&format=json', $url, $frbrUri, $query['query']['expression-frbr-uri']);
    $data = $this->client->downloadContent($url);
    $response = new AjaxResponse();
    $isOlder = $this->isAnOlderVersion($frbrUri, $query['query']['expression-frbr-uri']);
    $response->addCommand(new LoadDiffCommand(json_decode($data, TRUE), $isOlder));
    return $response;
  }

  /**
   * Determines if the expression is an older version than the compare to value.
   *
   * @param $frbrUri
   *   The FRBR URI of the legislation.
   * @param $expressionFrbrUri
   *   The FRBR URI of the expression we compare.
   *
   * @return bool
   */
  protected function isAnOlderVersion($frbrUri, $expressionFrbrUri) {
    $revisionPointInTime = $this->getDateByFrbrUri($frbrUri);
    $compareToPointInTime = $this->getDateByFrbrUri($expressionFrbrUri);
    return $compareToPointInTime > $revisionPointInTime;
  }

  /**
   * Returns the date of the legislation based on the FRBR URI.
   *
   * @param $frbrUri
   *   The FRBR URI of the legislation.
   *
   * @return string
   *   The date of the legislation.
   */
  public function getDateByFrbrUri($frbrUri) {
    $query = $this->entityTypeManager->getStorage('node')->getQuery()->allRevisions();
    $query->accessCheck();
    $query->condition('type', 'akn_legislation');
    $query->condition('field_frbr_uri', $frbrUri, 'LIKE');
    $rids = $query->execute();
    $revisions = $this->entityTypeManager->getStorage('node')->loadMultipleRevisions(array_keys($rids));
    $revision = reset($revisions);
    return $revision->get('field_legislation_date')->value;
  }

}
