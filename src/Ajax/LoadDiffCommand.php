<?php

namespace Drupal\unep_legislation\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for highlighting paragraphs.
 *
 * @ingroup ajax
 */
class LoadDiffCommand implements CommandInterface {

  protected $data;

  protected $is_older;

  /**
   * {@inheritdoc}
   */
  public function __construct($data, $is_older = FALSE) {
    $this->data = $data;
    $this->is_older = $is_older;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'aknLoadDiff',
      'data' => $this->data,
      'is_older' => $this->is_older ?? FALSE,
    ];
  }
}
