<?php

namespace Drupal\unep_legislation\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for highlighting paragraphs.
 *
 * @ingroup ajax
 */
class TagFilterCommand implements CommandInterface {

  /**
   * The tags.
   *
   * @var string
   */
  protected $tags;

  /**
   * The tags.
   *
   * @var string
   */
  protected $term;

  /**
   * {@inheritdoc}
   */
  public function __construct($tags, $term) {
    $this->tags = $tags;
    $this->term = $term;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'aknLegislationTagFilter',
      'highlights' => $this->tags,
      'term' => $this->term,
    ];
  }
}
