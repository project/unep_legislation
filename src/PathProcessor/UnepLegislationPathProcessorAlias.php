<?php

namespace Drupal\unep_legislation\PathProcessor;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\node\NodeInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\path_alias\PathProcessor\AliasPathProcessor;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the inbound/outbound path using path alias lookups.
 */
class UnepLegislationPathProcessorAlias extends AliasPathProcessor {

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * UnepLegislationPathProcessorAlias constructor.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   */
  public function __construct(AliasManagerInterface $alias_manager, LanguageManagerInterface $languageManager, UnepUtils $unepUtils) {
    parent::__construct($alias_manager);
    $this->unepUtils = $unepUtils;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    $langcode = isset($options['language']) ? $options['language']->getId() : $this->languageManager->getCurrentLanguage()->getId();
    $matches = $this->matchNodePath($path);

    if (!empty($matches['tab'])) {
      $alias = $this->unepUtils->getLatestFrbrUriForNode($matches['nid'], $langcode);
      return "{$alias}/{$matches['tab']}";
    }
    if (!empty($matches['rid'])) {
      $alias = $this->unepUtils->getRevisionFrbrUriForNode($matches['nid'], $matches['rid'], $langcode);
      if (!empty($alias)) {
        return $alias;
      }
    }

    if (!empty($matches['nid'])) {
      $alias = $this->unepUtils->getLatestFrbrUriForNode($matches['nid'], $langcode);
      if (!empty($alias)) {
        return $alias;
      }
    }

    return parent::processOutbound($path, $options, $request, $bubbleable_metadata);
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    preg_match('/^(?<frbr_uri>\/akn(.?)*)\/(?<tab>history|overview)$/', $path, $matches);
    if (!empty($matches['tab']) && !empty($matches['frbr_uri'])) {
      $node = $this->unepUtils->getRevisionFromFrbrUri($matches['frbr_uri']);
      if ($node instanceof NodeInterface) {
        return "/node/{$node->id()}/{$matches['tab']}";
      }
      return parent::processInbound($path, $request);
    }
    $node = $this->unepUtils->getLatestRevisionFromWorkFrbrUri($path) ?: $this->unepUtils->getRevisionFromFrbrUri($path);
    if ($node instanceof NodeInterface) {
      return ($node->isDefaultRevision())
        ? "/node/{$node->id()}"
        : "/node/{$node->id()}/revisions/{$node->getRevisionId()}/view";
    }

    return parent::processInbound($path, $request);
  }

  /**
   * Matches the path against each possible patterns.
   *
   * @param string $path
   *   The path to match.
   *
   * @return array
   *   An array with 'nid', 'rid', 'history', and 'overview' keys.
   */
  protected function matchNodePath($path) {
    preg_match('/^\/node\/(?<nid>[0-9]+)\/(?<tab>history|overview)$/', $path, $matches);
    if (!empty($matches['tab'])) {
      return $matches;
    }
    $matches = [];
    preg_match('/^\/node\/(?<nid>[0-9]+)(\/revisions\/(?<rid>[0-9]+)\/view)*$/', $path, $matches);
    return $matches;
  }

}
