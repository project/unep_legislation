<?php

namespace Drupal\unep_legislation\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\unep_legislation\UnepUtils;

/**
 * Checks access for displaying a tab.
 */
class AknAccessCheck implements AccessInterface {

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new AknAccessCheck.
   *
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *    The route match.
   */
  public function __construct(UnepUtils $unepUtils, RouteMatchInterface $routeMatch) {
    $this->unepUtils = $unepUtils;
    $this->routeMatch = $routeMatch;
  }

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $node = $this->routeMatch->getParameter('node');
    $isVisible = !(!$node->isPublished()) || $account->hasPermission('view any unpublished content');
    return AccessResult::allowedIf(
      $this->unepUtils->hasAknUri($node)
      && $this->unepUtils->isAknUri($node->get('field_frbr_uri')->value)
      && $isVisible);
  }

}
