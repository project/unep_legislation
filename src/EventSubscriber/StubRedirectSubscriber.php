<?php

namespace Drupal\unep_legislation\EventSubscriber;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirect stub subscriber.
 */
class StubRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * Constructor.
   */
  public function __construct(UnepUtils $unepUtils) {
    $this->unepUtils = $unepUtils;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::REQUEST] = ['onRequest'];
    $events[KernelEvents::RESPONSE] = ['onResponse'];
    return $events;
  }

  /**
   * A method to be called whenever a kernel.request event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event triggered by the request.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function onRequest(RequestEvent $event) {
    $this->processEvent($event);
  }

  /**
   * Kernel response event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   Response event.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function onResponse(ResponseEvent $event) {
    $this->processEvent($event);
  }

  /**
   * Redirects stub entities.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent|\Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  private function processEvent(RequestEvent|ResponseEvent $event) {
    $request = $event->getRequest();
    $route = $request->get('_route');
    if (empty($route) || !preg_match('/^entity\.(.+)\.canonical$/', $route)) {
      return;
    }
    $entity = $request->get('node');
    if (!$entity instanceof NodeInterface) {
      return;
    }
    if (!$this->unepUtils->hasAknUri($entity)
      || !$this->unepUtils->isAknUri($entity->get('field_frbr_uri')->value)
      || !$this->unepUtils->isStub($entity)) {
      return;
    }

    $uri = Url::fromRoute('unep_legislation.overview', [
      'node' => $entity->id(),
    ])->toString();
    $response = new TrustedRedirectResponse($uri);
    $event->setResponse($response);
  }

}
