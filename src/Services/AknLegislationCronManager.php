<?php

namespace Drupal\unep_legislation\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\unep_legislation_indigo\Client\IndigoV3;
use Drupal\unep_legislation_indigo\Client\Work;
use Drupal\unep_legislation_indigo\Drupal\DrupalImport;

/**
 * Service for AKN legislation cron manager.
 */
class AknLegislationCronManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Drupal Indigo importer.
   *
   * @var \Drupal\unep_legislation_indigo\Drupal\DrupalImport
   */
  protected $indigoImporter;

  /**
   * A config object for the system performance configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new AknLegislationCronManager.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\unep_legislation_indigo\Drupal\DrupalImport $indigoImporter
   *   The Drupal Indigo importer.
   */
  public function __construct(ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler, LoggerChannelFactoryInterface $logger, EntityTypeManagerInterface $entityTypeManager, DrupalImport $indigoImporter) {
    $this->config = $configFactory->get('unep_legislation_indigo.settings');
    $this->moduleHandler = $moduleHandler;
    $this->indigoImporter = $indigoImporter;
    $this->logger = $logger->get('unep_legislation');
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Run cron job.
   */
  public function runCron() {
    // @todo: Add cron job per provider.
    if (!$this->config->get('enable_indigo_import')
      || !$this->entityTypeManager->hasDefinition('indigo_data_provider')) {
      return;
    }
    $indigoStorage = $this->entityTypeManager->getStorage('indigo_data_provider');
    $providers = $indigoStorage->loadByProperties([
      'status' => 1,
    ]);
    foreach ($providers as $provider_name => $provider) {
      $uris = [];
      $this->moduleHandler->invokeAll('taxonomy_topics_work_expressions_list', [&$uris, $provider_name]);
      $client = new IndigoV3($provider->get('api_url'), $provider->get('api_key'), $this->logger, $provider->id());
      foreach ($uris as $uri) {
        $regex = '/\/taxonomy-topics\/(?<slug>(?:.)*)\/work-expressions?.*/';
        preg_match($regex, $uri, $matches);
        $slug = $matches['slug'] ?? NULL;
        try {
          $works = $client->getLegislations($uri, $slug);
          $this->moduleHandler->invokeAll('preprocess_work_expressions_list', [&$works, $slug]);
          $message = sprintf('Importing %s works from %s with endpoint %s', count($works), $provider->id(), $uri);
          $this->logger->notice($message);
          foreach ($works as &$work) {
            $work = $client->getWork($work->frbr_uri);
            if (!$work) {
              continue;
            }
            $work->addSlugToTaxonomyTopicsList($slug);
            $this->indigoImporter->importWork($work);
          }
        }
        catch (\Exception $e) {
          $this->logger->error('There was an error during the import' . $e);
        }
      }
    }
  }
}
