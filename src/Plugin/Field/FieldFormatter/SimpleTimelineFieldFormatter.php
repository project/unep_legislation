<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation\UnepUtils;
use Drupal\unep_legislation_indigo\Drupal\DrupalImport;
use Drupal\unep_legislation_indigo\Drupal\ImportUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'simple_timeline_text_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_timeline_text_formatter",
 *   label = @Translation("Simple timeline formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class SimpleTimelineFieldFormatter extends TimelineFieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = $comparing = [];
    $node = $items->getParent()->getValue();
    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->formatTimeline($item->value, $node, $langcode, $comparing);;
      if ($this->getSetting('compare') && $comparing) {
        $elements[$delta]['comparing'] = [
          '#theme' => 'diff_compare_to',
          '#title' => $this->t('Compare to'),
          '#items' => $comparing,
        ];
      }
    }
    return $elements;
  }

  protected function formatTimeline($value, NodeInterface $node, $langcode, &$comparing = []): array {
    $data = json_decode($value);
    if (empty($data->timeline)) {
      return [];
    }
    $language = $this->languageManager->getLanguage($langcode)->getName();
    $activeItem = $this->t('Select');
    $items = $comparing = [];
    foreach($data->timeline as $item) {
      $revision = $this->indigoImporter->getNodeRevisionByDate($node, $item->date);
      if ($revision) {
        $this->compareTo($comparing, $revision, $node, $langcode);
        $date = date('d F, Y', strtotime($item->date));
        $activeItem = ($revision->getRevisionId() == $node->getRevisionId()) ? sprintf('%s - %s', $date, $language) : $activeItem;
        $items[$item->date] = [
          'title' => strtotime($item->date),
          'language' => $langcode,
          'link' => Url::fromUserInput(ImportUtil::formatLegislationNodeLink($revision))->toString(),
          'active' => $revision->getRevisionId() == $node->getRevisionId(),
        ];
      }
    }
    return [
      '#theme' => 'legislation_select',
      '#active_item' => $activeItem,
      '#items' => $items,
      '#wrapper_id' => Html::getUniqueId('simple-timeline'),
      '#attributes' => ['class' => 'akoma-ntoso-select'],
      '#wrapper_attributes' => ['class' => 'akoma-ntoso'],
    ];
  }

}
