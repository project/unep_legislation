<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation\UnepUtils;
use Drupal\unep_legislation_indigo\Drupal\DrupalImport;
use Drupal\unep_legislation_indigo\Drupal\ImportUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'timeline_text_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "timeline_text_formatter",
 *   label = @Translation("Legislation timeline formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class TimelineFieldFormatter extends TimelineFieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = $comparing = [];
    $node = $items->getParent()->getValue();
    foreach ($items as $delta => $item) {
      $value = $item->value;
      $elements[$delta] = [
        '#theme' => 'item_list',
        '#list_type' => 'ol',
        '#items' => $this->formatTimeline($value, $node, $langcode, $comparing),
        '#attributes' => ['class' => 'akoma-ntoso-list'],
        '#wrapper_attributes' => ['class' => 'akoma-ntoso'],
      ];
      if ($this->getSetting('compare') && $comparing) {
        $elements[$delta]['comparing'] = [
          '#theme' => 'diff_compare_to',
          '#title' => $this->t('Compare to'),
          '#links' => $comparing,
        ];
      }
    }
    return $elements;
  }

}
