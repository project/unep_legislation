<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'pp_terms_text_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pp_terms_text_formatter",
 *   label = @Translation("PoolParty terms formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class PPTermsFieldFormatter extends FormatterBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'link_to_entity' => FALSE,
        'interactive' => FALSE,
        'markup_machine_name' => 'field_markup',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [
      '#attached' => [
        'library' =>  ['unep_legislation/poolparty_terms_tagging'],
      ],
    ];
    if ($this->getSetting('interactive')) {
      $elements['#attached']['drupalSettings']['interactive'] = FALSE;
    }
    $idsOrder = $this->getAvailableIds($items->getEntity());
    foreach ($items as $delta => $item) {
      $value = $item->value;
      $elements[$delta] = [
        '#theme' => 'item_list',
        '#list_type' => 'ol',
        '#items' => $this->formatTerms($value, $langcode, $idsOrder),
        // Class visually-hidden will be removed with JS when page is fully loaded.
        '#attributes' => ['class' => ['pp-terms-list', 'js-opacity-0']],
        '#wrapper_attributes' => ['class' => ['akoma-ntoso-terms']],
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  private function formatTerms($value, $langcode, $idsOrder): array {
    $data = json_decode($value);
    if (empty($data)) {
      return [];
    }
    $list = $sorted = [];
    foreach ($data as $anchor => $terms) {
      foreach ($terms as $term) {
        if (empty($term->root)) {
          continue;
        }
        if (empty($sorted[$anchor][$term->root->tid])) {
          $sorted[$anchor][$term->root->tid] = [
            'label' => [
              '#type' => 'markup',
              '#markup' => new TranslatableMarkup('<span class="parent-term">@label</span>', [
                '@label' => !empty($term->root->labels->$langcode) ? $term->root->labels->$langcode : $term->root->labels->en,
              ]),
            ],
            'children' => [
              '#theme' => 'item_list',
              '#list_type' => 'ul',
              '#attributes' => [
                'class' => ['list-group-top-concepts'],
              ],
              '#context' => ['list_style' => 'comma-list'],
              '#items' => [],
            ],
          ];
          // In the interactive mode, show only first group. The other groups
          // are shown when user interacts with the page.
          if (($this->getSetting('interactive') && count($sorted[$anchor]) > 1)) {
            $sorted[$anchor][$term->root->tid]['#wrapper_attributes']['class'][] = 'visually-hidden';
          }
        }
        $sorted[$anchor][$term->root->tid]['children']['#items'][$term->tid] = $this->buildTermLabel($term, $langcode);
      }
      // In the interactive mode, include a "See more".
      if ($this->getSetting('interactive') && count($sorted[$anchor]) > 1) {
        $sorted[$anchor]['see_more'] = [
          '#type' => 'markup',
          '#markup' => new TranslatableMarkup('<span class="see-more">See @count more</span>', [
            '@count' => count($sorted[$anchor]) - 1,
          ]),
          '#wrapper_attributes' => ['class' => ['interactive-mode--see-more']],
        ];
      }
      $list[$anchor] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#attributes' => ['class' => ['pp-terms']],
        '#wrapper_attributes' => [
          'anchor' => '#' . $anchor,
          'class' => [
            ($this->getSetting('interactive')) ? 'interactive-mode' : '',
          ],
        ],
        '#items' => $sorted[$anchor],
      ];
    }
    foreach ($list as $itemId => $item) {
      if (!in_array($itemId, $idsOrder)) {
        unset($list[$itemId]);
      }
    }
    if ($idsOrder) {
      uksort($list, function ($a, $b) use ($idsOrder) {
        return ((array_search($a, $idsOrder) > array_search($b, $idsOrder)) ? 1 : -1);
      });
    }
    return $list;
  }

  /**
   * Get a list with ids from markup.
   *
   * Sometimes terms are not returned in correct order from API.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity object.
   *
   * @return array
   *   Array with ids.
   */
  protected function getAvailableIds(FieldableEntityInterface $entity) {
    $field = $this->getSetting('markup_machine_name');
    if (!$entity->hasField($field)
      || $entity->get($field)->isEmpty()) {
      return [];
    }
    $markup = $entity->get($field)->value;
    $dom = new \DOMDocument();
    @$dom->loadHTML('<?xml encoding="UTF-8">' . $markup);
    $xpath = new \DOMXPath($dom);
    $query = sprintf("//*[@id]");
    $list = $xpath->query($query);
    $ids = [];
    foreach ($list as $value) {
      $ids[] = urldecode($value->getAttribute('id'));
    }
    return $ids;
  }

  /**
   * Builds a render array for a child term label.
   *
   * @param object $term
   *   The term object.
   * @param string $langcode
   *   The language code.
   *
   * @return array
   *   A render array representing the child term label.
   */
  private function buildTermLabel($term, $langcode) {
    $child_label = !empty($term->labels->$langcode) ? $term->labels->$langcode : $term->labels->en;

    if ($this->getSetting('link_to_entity')) {
      return [
        '#type' => 'link',
        '#title' => $child_label,
        '#url' => Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->tid]),
      ];
    } else {
      return [
        '#type' => 'markup',
        '#markup' => $child_label,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['link_to_entity'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Link label to the referenced entity'),
      '#default_value' => $this->getSetting('link_to_entity'),
    ];

    $elements['interactive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use interactive mode'),
      '#default_value' => $this->getSetting('interactive'),
    ];

    $elements['markup_machine_name'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Order PP term based on their position in markup'),
      '#description' => $this->t('Please use the correct field. Default is "field_markup".'),
      '#default_value' => $this->getSetting('markup_machine_name'),
      '#rows' => 1,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSettings();
    if (!empty($settings['link_to_entity'])) {
      $summary[] = $this->t('Link to the referenced entity');
    }
    if (!empty($settings['interactive'])) {
      $summary[] = $this->t('Use the interactive mode');
    }

    return $summary;
  }

}
