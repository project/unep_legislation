<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'legislation_text_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "legislation_text_formatter",
 *   label = @Translation("Legislation formatter"),
 *   field_types = {
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class LegislationFormatter extends FormatterBase {

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * Constructs a LegislationFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, UnepUtils $unepUtils) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->unepUtils = $unepUtils;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('unep_legislation.utils'),
    );
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $entity = $items->getEntity();
    $elements = [
      '#attached' => ['library' =>  ['unep_legislation/indigo_legislation']],
    ];
    $disableTooltips = $this->unepUtils->getAknNodeSetting($entity, 'akn_disable_tooltips');
    $disableTooltips = reset($disableTooltips);
    $elements['#attached']['drupalSettings']['disable_tooltips'] = $disableTooltips ?? FALSE;
    foreach ($items as $delta => $item) {
      $value = $item->value;
      $elements[$delta] = [
        '#prefix' => '<div class="akoma-ntoso akoma-ntoso-body decorate-remarks">',
        '#type' => 'markup',
        '#markup' => $value,
        '#suffix' => '</div>'
      ];
    }

    return $elements;
  }
}
