<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'toc_text_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "toc_text_formatter",
 *   label = @Translation("Legislation Table of contents formatter"),
 *   field_types = {
 *     "string_long",
 *   }
 * )
 */
class TOCFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [
      '#attached' => [
        'library' =>  ['unep_legislation/table_of_contents'],
      ],
    ];
    foreach ($items as $delta => $item) {
      $value = $item->value;
      $elements[$delta] = [
        'filters' => [
          '#type' => 'container',
          '#attributes' => ['class' => ['filters--toc']],
          'search' => [
            '#type' => 'textfield',
            '#title' => 'Input',
            '#label_display' => 'hidden',
            '#placeholder' => 'Search the table of contents',
            '#attributes' => [
              'class' => ['filters--toc--input'],
              'id' => 'filters--toc--filter',
            ],
            '#wrapper_attributes' => ['class' => ['filters--toc--input-wrapper']],
          ],
          'reset' => [
            '#type' => 'submit',
            '#value' => $this->t('Reset'),
            '#attributes' => [
              'class' => ['btn', 'btn--toc', 'btn--toc--reset'],
              'id' => 'tocSearchBtnReset',
            ]
          ],
          'btn_expand' => [
            '#type' => 'button',
            '#value' => $this->t('Expand all'),
            '#attributes' => [
              'class' => ['btn', 'btn--toc', 'btn--toc--expand'],
              'id' => 'tocBtnExpand',
            ]
          ],
          'btn_collapse' => [
            '#type' => 'button',
            '#value' => $this->t('Collapse all'),
            '#attributes' => [
              'class' => ['btn', 'btn--toc', 'btn--toc--collapse'],
              'id' => 'tocBtnCollapse',
            ]
          ],
        ],
        'list' => [
          '#theme' => 'item_list',
          '#list_type' => 'ol',
          '#items' => $this->formatToC($value),
          '#attributes' => [
            'class' => ['panel'],
          ],
          '#wrapper_attributes' => ['class' => ['akoma-ntoso', 'akoma-ntoso-toc']],
        ]
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  private function formatToC($value): array {
    $data = json_decode($value);
    if (empty($data->toc)) {
      return [];
    }
    $level = 3;
    $itemsList = [];
    foreach ($data->toc as $section) {
      $itemList = [
        '#type' => 'link',
        '#title' => $section->title,
        '#url' => Url::fromUserInput(self::fixURL($section->url)),
        '#attributes' => ['class' => ['akoma-ntoso-item-link']],
      ];
      if (!empty($section->children)) {
        $subList = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $this->renderChildren($section->children, $level + 1),
          '#attributes' => [
            'class' => ['akoma-ntoso-list']
          ],
        ];
        $itemList['#attributes']['class'][] = 'visually-hidden';
        $itemList['sections'] = $subList;
        $itemsList[] = [
          '#type' => 'details',
          '#title' => [
            '#type' => 'link',
            '#title' => $section->title,
            '#url' => Url::fromUserInput(self::fixURL($section->url)),
            '#attributes' => ['class' => ['akoma-ntoso-item-link']],
          ],
          $itemList
        ];
        continue;
      }
      $itemList['#attributes']['class'][] = 'akoma-ntoso-item-without-children';
      $itemsList[] = $itemList;
    }
    return $itemsList;
  }

  /**
   * {@inheritdoc}
   */
  private function renderChildren($children, $level): array {
    $itemsList = [];
    foreach ($children as $child) {
      $itemList = [
        '#type' => 'link',
        '#title' => $child->title,
        '#url' => Url::fromUserInput(self::fixURL($child->url)),
        '#attributes' => ['class' => ['akoma-ntoso-item-link']],
      ];
      if (!empty($child->children)) {
        $subList = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => $this->renderChildren($child->children, $level + 1),
          '#attributes' => ['class' => 'akoma-ntoso-list'],
        ];
        $itemList['#attributes']['class'][] = 'visually-hidden';
        $itemList['sections'] = $subList;
        $itemsList[] = [
          '#type' => 'details',
          '#title' => [
            '#type' => 'link',
            '#title' => $child->title,
            '#url' => Url::fromUserInput(self::fixURL($child->url)),
            '#attributes' => ['class' => ['akoma-ntoso-item-link']],
          ],
          $itemList,
        ];
        continue;
      }
      $itemList['#attributes']['class'][] = 'akoma-ntoso-item-without-children';
      $itemsList[] = $itemList;
    }

    return $itemsList;
  }

  /**
   * {@inheritdoc}
   */
  private static function fixURL($url) {
    $url = preg_replace('/.*~/i', '#', $url);
    return urldecode($url);
  }
}
