<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'timeline_json_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "timeline_events_field_formatter",
 *   label = @Translation("Timeline Events Field Formatter"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class TimelineEventsFieldFormatter extends TimelineFieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $node = $items->getParent()->getValue();
    foreach ($items as $delta => $item) {
      $value = $item->value;
      $elements[$delta] = [
        '#theme' => 'history_tab_list',
        '#items' => $this->formatTimeline($value, $node, $langcode),
        '#node_title' => $node->getTitle(),
        '#node_language' => $node->language()->getName(),
        '#node_url' => Url::fromRoute('entity.node.canonical', ['node' => $node->id()])->toString(),
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formatTimeline($value, $node, $langcode, &$comparing = []): array {
    $data = json_decode($value);
    if (empty($data)) {
      return [];
    }
    $timeline = $data->timeline;
    if (empty($timeline)) {
      return [];
    }
    foreach ($timeline as &$item) {
      foreach ($item->events as &$event) {
        $revision = $this->indigoImporter->getNodeRevisionByDate($node, $item->date);
        $item->by_frbr_uri = ($revision) ? $revision->field_frbr_uri->value : NULL;
        $event->file = ($revision) ? $this->includeFiles($revision, $langcode) : NULL;
        if (!empty($event->by_frbr_uri)) {
          /** @var \Drupal\Core\Entity\RevisionableInterface $eventNode */
          $eventNode = $this->unepUtils->getLatestRevisionFromWorkFrbrUri($event->by_frbr_uri);
          $event->by_frbr_uri = NULL;
          if ($eventNode) {
            $event->by_frbr_uri = $eventNode->get('field_frbr_uri')->value;
            $file = $this->includeFiles($eventNode, $langcode);
          }
          $event->file = $eventNode ? $file : NULL;
        }
      }
    }
    return($timeline);
  }

  /**
   * {@inheritdoc}
   */
  protected function includeFiles($revision, $langcode, $fileOnly = TRUE) {
    $file = parent::includeFiles($revision, $langcode, $fileOnly);
    if (!$file) {
      return [];
    }
    return [
      'url' => $file->createFileUrl(),
      'size' => $file->getSize() !== NULL ? format_size($file->getSize()) : '',
      'extension' => strtoupper(pathinfo($file->getFilename(), PATHINFO_EXTENSION)),
    ];
  }

}
