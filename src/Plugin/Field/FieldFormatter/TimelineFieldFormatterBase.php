<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation\UnepUtils;
use Drupal\unep_legislation_indigo\Drupal\DrupalImport;
use Drupal\unep_legislation_indigo\Drupal\ImportUtil;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The base formatter for the timeline field.
 */
abstract class TimelineFieldFormatterBase extends FormatterBase {

  use StringTranslationTrait;
  /**
   * The entity type manager service.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Drupal Indigo importer.
   *
   * @var \Drupal\unep_legislation_indigo\Drupal\DrupalImport
   */
  protected $indigoImporter;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;
  
  /**
   * Constructs a TimelineFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\unep_legislation_indigo\Drupal\DrupalImport $indigoImporter
   *   The Drupal Indigo importer.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type interface service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $fileUrlGenerator
   *   The file url generator service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DrupalImport $indigoImporter, UnepUtils $unepUtils, LanguageManagerInterface $languageManager, EntityTypeManagerInterface $entityTypeManager, FileUrlGeneratorInterface $fileUrlGenerator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->indigoImporter = $indigoImporter;
    $this->unepUtils = $unepUtils;
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('unep_legislation_indigo.drupal_importer'),
      $container->get('unep_legislation.utils'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('file_url_generator'),
    );
  }

  /**
   * The default settings for the formatter.
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();
    $settings['compare'] = FALSE;
    $settings['include_files'] = FALSE;

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['compare'] = [
      '#title' => $this->t('Show "Compare to" button'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('compare'),
    ];
    $form['include_files'] = [
      '#title' => $this->t('Show files in timeline'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('include_files'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting('compare')) {
      $summary[] = $this->t('Show "Compare to" button.');
    }
    if ($this->getSetting('include_files')) {
      $summary[] = $this->t('Show files in timeline.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTimeline($value, NodeInterface $node, $langcode, &$comparing = []): array {
    $data = json_decode($value);
    if (empty($data->timeline)) {
      return [];
    }
    $itemsList = $comparing = [];
    foreach($data->timeline as $item) {
      $itemList = [
        'element' => [
          '#theme' => 'legislation_item_list',
          '#date' => [
            '#type' => 'html_tag',
            '#tag' => 'strong',
            '#value' => $item->date,
            '#attributes' => ['class' => ['akn-pit', 'title', 'date']],
          ],
        ],
      ];
      $revision = $this->indigoImporter->getNodeRevisionByDate($node, $item->date);
      if ($revision) {
        $this->compareTo($comparing, $revision, $node, $langcode);
        if ($revision->getRevisionId() != $node->getRevisionId()) {
          $itemList['element']['#revision'] = [
            '#type' => 'link',
            '#title' => $revision->isLatestRevision() ? $this->t('Latest') : $this->t('View'),
            '#url' => Url::fromUserInput(ImportUtil::formatLegislationNodeLink($revision)),
            '#attributes' => [
              'class' => [
                'view-revision',
                ($revision->isLatestRevision() ? 'latest-revision' : ''),
              ],
            ],
          ];
        }
        else {
          $itemList['element']['#revision'] = [
            '#type' => 'html_tag',
            '#tag' => 'strong',
            '#value' => new TranslatableMarkup('Currently viewing', [], ['context' => 'unep_legislation_timeline']),
            '#attributes' => ['class' => ['current-viewing']],
          ];
          $itemList['#wrapper_attributes']['class'][] = 'current-viewing';
          $itemList['element']['#current_viewing'] = TRUE;
        }
        $itemList['element']['#latest_revision'] = $revision->isLatestRevision();
        if ($this->getSetting('include_files') && !$revision->get('field_files')->isEmpty()) {
          $itemList['element']['#file'] = $this->includeFiles($revision, $langcode);
        }
      }
      $subListItems = [];
      foreach($item->events as $event) {
        $subListItems[] = $this->formatEvent($event);
      }
      $subList = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $subListItems,
        '#attributes' => ['class' => 'akoma-ntoso-list'],
      ];
      $itemList['element']['#events'] = $subList;
      $itemsList[] = $itemList;
    }
    return $itemsList;
  }

  /**
   * Create a list with points in time to compare.
   *
   * @param array $comparing
   *   Array of points in time.
   * @param \Drupal\Core\Entity\RevisionableInterface $revision
   *   A revision to compare.
   * @param \Drupal\node\NodeInterface $node
   *   Current node.
   * @param string $langcode
   *   The language that should be used to render the field.
   */
  protected function compareTo(array &$comparing, RevisionableInterface $revision, NodeInterface $node, $langcode) {
    if ($revision->getRevisionId() == $node->getRevisionId()) {
      return;
    }
    $date = date('Y-m-d', strtotime($revision->field_legislation_date->value));
    if ($node->isLatestRevision()) {
      $url = Url::fromRoute('unep_legislation.diff_api', [
        'node' => $node->id(),
        'query' => [
          'expression-frbr-uri' => $revision->field_frbr_uri->value,
        ],
      ]);
    } else {
      $url = Url::fromRoute('unep_legislation.revision.diff_api', [
        'node' => $node->id(),
        'node_revision' => $node->getRevisionId(),
        'query' => [
          'expression-frbr-uri' => $revision->field_frbr_uri->value,
        ],
      ]);
    }
    $comparing[$date] = [
      'title' => strtotime($revision->field_legislation_date->value),
      'language' => $langcode,
      'link' => $url->toString(),
    ];
  }

  /**
   * Include file in timeline.
   *
   * @param \Drupal\Core\Entity\RevisionableInterface $revision
   *   A revision to compare.
   * @param string $langcode
   *   The language that should be used to render the file.
   * @param bool $fileOnly
   *   Whether only the file should be returned for further processing or a
   *   markup link.
   *
   * @return array
   *   An array representing file information.
   */
  protected function includeFiles(RevisionableInterface $revision, string $langcode, bool $fileOnly = FALSE) {
    if ($revision->hasTranslation($langcode)) {
      $revision = $revision->getTranslation($langcode);
    }
    if ($revision->get('field_files')->isEmpty()) {
      return [];
    }
    $file = $revision->get('field_files')->first()->entity;
    if (!$file) {
      return [];
    }
    if ($fileOnly) {
      return $file;
    }
    return [
      '#type' => 'link',
      '#title' => $this->t('File'),
      '#url' => Url::fromUserInput($file->createFileUrl()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected static function fixURL($url) {
    $url = str_replace('https://informea.indigo.ipbes.net/api/v2/akn/al/act/2020/155/sqi@2021-01-08/!main~', '#', $url);
    return $url;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatEvent($event) {
    $ret = [];
    switch($event->type) {
      case 'commencement':
        $ret = new TranslatableMarkup('<span>Commenced</span>', [], ['context' => 'unep_legislation_timeline']);
        break;
      case 'consolidation':
        $ret = new TranslatableMarkup('<span>Consolidation</span>', [], ['context' => 'unep_legislation_timeline']);
        break;
      case 'publication':
        $ret = new TranslatableMarkup('<span>Published</span>', [], ['context' => 'unep_legislation_timeline']);
        break;
      case 'amendment':
        if (!empty($event->by_frbr_uri)) {
          $ret = [
            'type' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => new TranslatableMarkup('@description', [
                '@description' => $event->description,
              ]),
              '#attributes' => ['class' => ['info', 'amendment']],
            ],
            'link' => $this->getRelationshipMarkup($event),
          ];
        }
        break;
      default:
        if (!empty($event->by_frbr_uri)) {
          $ret = [
            'type' => [
              '#type' => 'html_tag',
              '#tag' => 'span',
              '#value' => new TranslatableMarkup('@type', [
                '@type' => $event->description ?? $event->type,
              ]),
              '#attributes' => ['class' => ['info']],
            ],
            'link' => $this->getRelationshipMarkup($event),
          ];
        }
    }
    return $ret;
  }

  /**
   * Prepare a renderable array.
   *
   * @param object $event
   *   Object representing a legislation.
   *
   * @return array
   *   Return a renderable array with link if legislation exists, otherwise
   *   return a markup with a title in bold.
   */
  protected function getRelationshipMarkup(object $event) {
    $linkExists = $this->unepUtils->getLatestRevisionFromWorkFrbrUri($event->by_frbr_uri);
    if ($linkExists) {
      return [
        '#type' => 'link',
        '#title' => $event->by_title,
        '#url' => Url::fromUserInput(self::fixURL($event->by_frbr_uri)),
        '#attributes' => ['class' => ['relation',$event->type]],
      ];
    }
    return [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => new TranslatableMarkup('@title', [
        '@title' => $event->by_title,
      ]),
      '#attributes' => ['class' => ['relation']],
    ];
  }

}
