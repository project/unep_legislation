<?php

namespace Drupal\unep_legislation\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\node\NodeInterface;

/**
 * Plugin implementation of the 'akn_download_files_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "legislation_download_files_formatter",
 *   label = @Translation("Download files formatter"),
 *   field_types = {
 *     "file",
 *   }
 * )
 */
class DownloadFilesFieldFormatter extends TimelineFieldFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $node = $items->getParent()->getValue();
    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->formatFiles($items->getName(), $node, $langcode);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatFiles($fieldName, NodeInterface $node, $langcode): array {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $rIds = $nodeStorage->revisionIds($node);
    $activeItemUrl = NULL;
    $activeItem = $this->t('Select');
    $items  = [];
    foreach ($rIds as $rid) {
      $revision = $nodeStorage->loadRevision($rid);
      $file = $revision->get($fieldName)->entity;
      $versionDate = $revision->get('field_legislation_date')->value;
      $date = date('d F, Y', strtotime($versionDate));
      $url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
      $activeItem = ($revision->getRevisionId() == $node->getRevisionId())
        ? sprintf('(%s, %s)', strtoupper(pathinfo($file->getFilename(), PATHINFO_EXTENSION)), format_size($file->getSize()))
        : $activeItem;
      $activeItemUrl = ($revision->getRevisionId() == $node->getRevisionId()) ? $url : $activeItemUrl;
      $items[$date] = [
        'title' => strtotime($versionDate),
        'language' => strtoupper($langcode),
        'link' => $url,
        'active' => $revision->getRevisionId() == $node->getRevisionId(),
      ];
    }
    return [
      '#theme' => 'legislation_download',
      '#active_item' => $activeItem,
      '#active_item_url' => $activeItemUrl ?? '#',
      '#items' => array_reverse($items),
      '#wrapper_id' => Html::getUniqueId('work-files-download'),
      '#attributes' => ['class' => 'akoma-ntoso-select'],
      '#wrapper_attributes' => ['class' => 'akoma-ntoso'],
    ];
  }

}
