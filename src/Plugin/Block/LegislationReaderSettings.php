<?php

namespace Drupal\unep_legislation\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\indigo_data_provider\Entity\IndigoDataProvider;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Legislation Reader Settings" block.
 *
 * @Block(
 *   id = "legislation_reader_settings",
 *   admin_label = @Translation("Legislation Reader Settings"),
 *   category = @Translation("UNEP Legislation")
 * )
 */
class LegislationReaderSettings extends BlockBase implements ContainerFactoryPluginInterface{

  use StringTranslationTrait;

  /**
   * A config object for the system performance configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * LatestVersionBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, RouteMatchInterface $routeMatch, UnepUtils $unepUtils, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config_factory->get('unep_legislation_indigo.settings');
    $this->routeMatch = $routeMatch;
    $this->unepUtils = $unepUtils;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('unep_legislation.utils'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'show_tags' => TRUE,
      'show_provisions' => FALSE,
      'options' => [
        'highlight' => TRUE,
      ],
      'show_inline_diff' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['show_tags'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show tags'),
      '#default_value' => $this->configuration['show_tags'],
    ];
    $form['show_provisions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show changes'),
      '#description' => $this->t('Show provisions that have been amended. Please make sure to enable "Show tags"'),
      '#default_value' => $this->configuration['show_provisions'],
    ];
    $form['options']['highlight'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Highlight editorial remarks'),
      '#default_value' => $this->configuration['options']['highlight'],
    ];
    $form['show_inline_diff'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show changes side-by-side'),
      '#description' => $this->t('Show changes in a side-by-side view.'),
      '#default_value' => $this->configuration['show_inline_diff'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $options = $form_state->getValue('options');
    $this->configuration['show_tags'] = $form_state->getValue('show_tags');
    $this->configuration['show_provisions'] = $form_state->getValue('show_provisions');
    $this->configuration['show_inline_diff'] = $form_state->getValue('show_inline_diff');
    $this->configuration['options']['highlight'] = $options['highlight'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // For routes like Layout builder.
    if (!$this->routeMatch->getParameter('node')) {
      return [];
    }

    $build = [
      '#theme' => 'legislation_reader_settings',
      '#options' => $this->configuration['options'],
      '#show_tags' => $this->configuration['show_tags'] && $this->legislationHasEnrichments(),
      '#show_provisions' => ($this->configuration['show_provisions'] && $this->legislationHasAmendments()),
      '#show_inline_diff' => $this->configuration['show_inline_diff'],
      '#attributes' => [
        'class' => 'legislation-reader-settings',
      ],
      '#attached' => [
        'library' =>  ['unep_legislation/legislation_reader_settings'],
      ],
    ];
    if ($this->configuration['options']) {
      $build['#attached']['drupalSettings']['reader_settings']['options'] = $this->configuration['options'];
    }
    $build['#attached']['drupalSettings']['reader_settings']['show_tags'] = ($this->configuration['show_tags']);
    $build['#attached']['drupalSettings']['reader_settings']['show_provisions'] = ($this->configuration['show_provisions'] && $this->legislationHasAmendments());
    $build['#attached']['drupalSettings']['reader_settings']['show_inline_diff'] = ($this->configuration['show_inline_diff']);
    if ($this->config->get('indigo_partner')) {
      $build['#attached']['drupalSettings']['indigo_partner'] = $this->config->get('indigo_partner');
    }
    $node = $this->routeMatch->getParameter('node');
    $provider = $this->unepUtils->getNodeProvider($node);
    if ($provider instanceof IndigoDataProvider) {
      $build['#attached']['drupalSettings']['indigo_url'] = $provider->get('url');
    }
    $build['#attached']['drupalSettings']['reader_settings']['frbr_uri'] = $node->get('field_frbr_uri')->value;

    return $build;
  }

  /**
   * Check if legislation has amendments. If FALSE, do not show provisions.
   *
   * @return bool
   *   TRUE if legislation has amendments, FALSE otherwise.
   */
  protected function legislationHasAmendments() {
    $node = $this->routeMatch->getParameter('node');
    return $this->unepUtils->legislationHasAmendments($node);
  }

  /**
   * Check if legislation has enrichments.
   *
   * Depends on the module that implements the hook. Defaults to TRUE.
   *
   * @return bool
   *   TRUE if legislation has enrichments, FALSE otherwise. Defaults to TRUE
   *   for backward compatibility.
   */
  protected function legislationHasEnrichments() {
    $node = $this->routeMatch->getParameter('node');
    $hasEnrichments = TRUE;
    $this->moduleHandler->invokeAll('preprocess_node_has_enrichments', [$node, &$hasEnrichments]);
    return $hasEnrichments;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }
}
