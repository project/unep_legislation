<?php

namespace Drupal\unep_legislation\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Related documents" block.
 *
 * @Block(
 *   id = "legislation_related_documents_block",
 *   admin_label = @Translation("Related documents & Subsidiary"),
 *   category = @Translation("UNEP Legislation")
 * )
 */
class RelatedDocumentsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * RelatedDocumentsBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $routeMatch, UnepUtils $unepUtils) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->unepUtils = $unepUtils;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('unep_legislation.utils'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // For routes like Layout builder.
    $node = $this->routeMatch->getParameter('node');
    if (!$node) {
      return [];
    }

    $relateRelationship = [];
    foreach ($this->unepUtils->getEventAllAppearances($node) as $relation) {
      if ($this->unepUtils->legislationHasAmendments($relation)) {
        $relateRelationship['amends'][] = [
          'url' => $this->unepUtils->getLatestFrbrUriForNode($relation->id(), 'en'),
          'title' => $relation->getTitle(),
        ];
      }
      if ($this->unepUtils->legislationHasEventsByType($relation, 'repeal')) {
        $relateRelationship['repeals'][] = [
          'url' => $this->unepUtils->getLatestFrbrUriForNode($relation->id(), 'en'),
          'title' => $relation->getTitle(),
        ];
      }
    }
    return [
      '#theme' => 'legislation_related_documents',
      '#amends' => $relateRelationship['amends'] ?? [],
      '#amended_by' => $this->unepUtils->getLegislationAmendedBy($node),
      '#repeals' => $relateRelationship['repeals'] ?? [],
      '#repealed_by' => $this->unepUtils->getLegislationRepealedBy($node),
      '#commenced_by' => [],
      '#subsidiary_legislation' => $this->unepUtils->getSubsidiaryLegislation($node->field_work_frbr_uri->value),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }
}
