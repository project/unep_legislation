<?php

namespace Drupal\unep_legislation\Plugin\Block;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation\Form\LinkagesForm;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Legislation Reader Settings" block.
 *
 * @Block(
 *   id = "alignments_and_linkages",
 *   admin_label = @Translation("Alignments and Linkages Block"),
 *   category = @Translation("UNEP Legislation")
 * )
 */
class LinkagesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * LatestVersionBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuidService
   *   The UUID service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $routeMatch, FormBuilderInterface $formBuilder, ModuleHandlerInterface $moduleHandler, UnepUtils $unepUtils, UuidInterface $uuidService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->formBuilder = $formBuilder;
    $this->moduleHandler = $moduleHandler;
    $this->unepUtils = $unepUtils;
    $this->uuidService = $uuidService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('form_builder'),
      $container->get('module_handler'),
      $container->get('unep_legislation.utils'),
      $container->get('uuid'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'show_only_terms' => FALSE,
      'block_id' => NULL,
      'block_fields' => '',
      'block_field_sort' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['show_only_terms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show only terms select'),
      '#description' => $this->t('Enable if you want only one select with all terms available.'),
      '#default_value' => $this->configuration['show_only_terms'],
    ];
    $form['block_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block unique ID'),
      '#description' => $this->t('Add a block ID if you want to display multiple blocks in same page.'),
      '#default_value' => !empty($this->configuration['block_id'])
        ? $this->configuration['block_id']
        : $this->configuration['id'] . ':' . $this->uuidService->generate(),
    ];
    $form['block_fields'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block fields'),
      '#description' => $this->t('Choose fields separated used for this block, separated by comma (E.g.: field_pp_climate_change,field_pp_goals).'),
      '#default_value' => $this->configuration['block_fields'],
    ];
    $form['block_field_sort'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block fields'),
      '#description' => $this->t('Choose field for sorting (E.g.: field_enrichments/field_plastic_tags).'),
      '#default_value' => $this->configuration['block_field_sort'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['show_only_terms'] = $form_state->getValue('show_only_terms');
    $this->configuration['block_id'] = $form_state->getValue('block_id');
    $this->configuration['block_fields'] = $form_state->getValue('block_fields');
    $this->configuration['block_field_sort'] = $form_state->getValue('block_field_sort');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // For routes like Layout builder.
    if (!$this->routeMatch->getParameter('node')) {
      return [];
    }

    return [
      '#theme' => 'alignments_and_linkages',
      '#form' => $this->formBuilder->getForm(LinkagesForm::class, $this->configuration),
      '#attached' => [
        'library' => [
          'unep_legislation/tag_filter',
        ],
      ],
    ];
  }

  /**
   * Indicates whether the block should be shown.
   *
   * Alignments and Linkages should appear if node is AKN, with a valid FRBR
   * URIs and has at least one field with data.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  protected function blockAccess(AccountInterface $account) {
    $fields = $this->configuration['block_fields'] ? explode(',', $this->configuration['block_fields']) : [];
    // Each project should define their own fields.
    $this->moduleHandler->invokeAll('alignments_and_linkages_fields', [&$fields, $this->configuration]);
    $node = $this->routeMatch->getParameter('node');
    if (!$node instanceof NodeInterface || !$fields) {
      return AccessResult::forbidden();
    }
    // Permissions at node level.
    if (!$this->blockIsVisible($node)) {
      return AccessResult::forbidden();
    }
    // At least one field should be with data.
    foreach ($fields as $field) {
      if (!$node->get($field)->isEmpty()) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }

  /**
   * Check if the block should be shown for a given node.
   *
   * @param $node
   *   The node.
   *
   * @return bool
   *   TRUE if the block should be shown, FALSE otherwise.
   */
  protected function blockIsVisible($node) {
    [, $blockUuid] = explode(':', $this->configuration['block_id']);
    $uuids = $this->unepUtils->getAknNodeSetting($node, $this->configuration['id']);
    // Default to true if no settings are set.
    if (empty($uuids)) {
      return TRUE;
    }
    if (in_array('*', $uuids)) {
      return FALSE;
    }
    return !in_array($blockUuid, $uuids);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }
}
