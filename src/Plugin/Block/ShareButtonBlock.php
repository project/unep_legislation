<?php

namespace Drupal\unep_legislation\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Share button" block.
 *
 * @Block(
 *   id = "legislation_share_button_block",
 *   admin_label = @Translation("Share AKN URL"),
 *   category = @Translation("UNEP Legislation")
 * )
 */
class ShareButtonBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * ShareButtonBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $routeMatch, UnepUtils $unepUtils) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->unepUtils = $unepUtils;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('unep_legislation.utils'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // For routes like Layout builder.
    $node = $this->routeMatch->getParameter('node');
    if (!$node) {
      return [];
    }
    return [
      '#theme' => 'legislation_share_button_block',
      '#attached' => [
        'library' =>  ['unep_legislation/share_button_block'],
      ],
    ];
  }

  public function access(AccountInterface $account, $return_as_object = FALSE) {
    $node = $this->routeMatch->getParameter('node');
    if (!$this->unepUtils->hasAknUri($node)) {
      return AccessResult::forbidden();
    }
    if (!$account->hasPermission('view akn_legislation revisions')
      || !$account->hasPermission('use akn share button')) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }
}
