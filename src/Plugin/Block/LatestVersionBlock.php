<?php

namespace Drupal\unep_legislation\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "Latest version" block.
 *
 * @Block(
 *   id = "legislation_latest_version_block",
 *   admin_label = @Translation("Latest Version Info"),
 *   category = @Translation("UNEP Legislation")
 * )
 */
class LatestVersionBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * LatestVersionBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $routeMatch, LanguageManagerInterface $languageManager, UnepUtils $unepUtils) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->languageManager = $languageManager;
    $this->unepUtils = $unepUtils;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('language_manager'),
      $container->get('unep_legislation.utils'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // For routes like Layout builder.
    $node = $this->routeMatch->getParameter('node');
    if (!$node) {
      return [];
    }
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $nid = $node->id();
    $revision = $this->routeMatch->getParameter('node_revision');
    $pointInTimeDate = $revision->field_legislation_date->value;
    $first_expression_date = date('Y-m-d', strtotime($pointInTimeDate));
    $second_expression_date = $this->unepUtils->getNextRevisionDateFromDate($nid, $first_expression_date);
    $build = [
      '#theme' => 'legislation_latest_version_block',
      '#attached' => [
        'library' =>  ['unep_legislation/latest_revision_block'],
      ],
      '#first_expression_date' => strtotime($first_expression_date),
      '#second_expression_date' => strtotime('-1 day', strtotime($second_expression_date)),
      '#info' => $this->t('You are viewing an older version of the document'),
      '#latest_revision' => Url::fromUserInput($this->unepUtils->getLatestFrbrUriForNode($nid, $langcode))->toString(),
      '#links' => [],
    ];
    return $build;
  }

  public function access(AccountInterface $account, $return_as_object = FALSE) {
    if ($this->routeMatch->getRouteName() != 'entity.node.revision') {
      return AccessResult::forbidden();
    }
    if (!$account->hasPermission('view akn_legislation revisions')) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }
}
