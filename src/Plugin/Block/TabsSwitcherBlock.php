<?php

namespace Drupal\unep_legislation\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\unep_legislation\UnepUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a "AKN Tabs Switche" block.
 *
 * @Block(
 *   id = "akn_legislation_tabs_switcher_block",
 *   admin_label = @Translation("AKN Tabs Switcher Block"),
 *   category = @Translation("UNEP Legislation")
 * )
 */
class TabsSwitcherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The module handler to invoke the alter hook.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The UNEP utils service.
   *
   * @var \Drupal\unep_legislation\UnepUtils
   */
  protected $unepUtils;

  /**
   * TabsSwitcherBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\unep_legislation\UnepUtils $unepUtils
   *   The UNEP utils service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ModuleHandlerInterface $moduleHandler, RouteMatchInterface $routeMatch, UnepUtils $unepUtils) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleHandler = $moduleHandler;
    $this->routeMatch = $routeMatch;
    $this->unepUtils = $unepUtils;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('current_route_match'),
      $container->get('unep_legislation.utils'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->routeMatch->getParameter('node');
    if (!$node) {
      return [];
    }
    $tabs = $this->unepUtils->getDefaultTabs();
    $this->moduleHandler->alter('unep_legislation_tabs', $tabs, $node);
    $allowedTabs = $this->unepUtils->getAknNodeSetting($node, 'akn_show_tabs');
    $items = [];
    foreach ($tabs as $key => $tab) {
      if (!empty($allowedTabs)
        && !in_array($key, $allowedTabs)
        && $key != 'entity.node.canonical') {
        continue;
      }
      if ($key != 'unep_legislation.overview' && $this->unepUtils->isStub($node)) {
        continue;
      }
      $items[$key] = [
        '#type' => 'link',
        '#url' => $this->generateTabUrl($key),
        '#title' => $tab,
        '#attributes' => [
          'class' => [
            'nav-link',
            $this->isCurrentTab($key) ? 'active is-active' : '',
            preg_replace('/\.|_/', '-', $key),
          ],
        ],
      ];
    }
    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

  /**
   * Checks if the tab is the active tab.
   *
   * @param string $currentRoute
   *   The name of the current route.
   *
   * @return bool
   *   TRUE if the current route is the active tab.
   */
  protected function isCurrentTab(string $currentRoute) {
    return ($this->routeMatch->getRouteName() == 'entity.node.revision' && $currentRoute == 'entity.node.canonical')
      || ($this->routeMatch->getRouteName() == $currentRoute);
  }

  /**
   * Creates the tab url.
   *
   * @param string $key
   *   The route id of the tab.
   *
   * @return mixed
   *   The url of the tab.
   */
  protected function generateTabUrl(string $key) {
    $node = $this->routeMatch->getParameter('node');
    if ($this->routeMatch->getRouteName() == 'entity.node.revision' && $key == 'entity.node.canonical') {
      $revision = $this->routeMatch->getParameter('node_revision');
      return Url::fromRoute('entity.node.revision', ['node' => $revision->id(), 'node_revision' => $revision->getRevisionId()]);
    }
    return Url::fromRoute($key, ['node' => $node->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    $node = $this->routeMatch->getParameter('node');
    return AccessResult::allowedIf(
      $node instanceof NodeInterface &&
      $this->unepUtils->hasAknUri($node)
      && $this->unepUtils->isAknUri($node->get('field_frbr_uri')->value));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url']);
  }
}
